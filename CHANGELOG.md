# Cargo – Changelog

All notable changes to Cargo will be documented in this file.
The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/).

## 3.0.1 - 2021-09-30

### Changed
- Updates dependencies


## 3.0.0 - 2020-11-22

### Added
- Support for Traefik 2
- OpenID Connect for authentication
- Support for selecting nodes by label per environment
- Option to specify full image in a deployment instead of tag only
- Setting to change the appearance of the tables
- Validation for frontend domains and reserved frontend names

### Changed
- Updates dependencies
- Encryption keys

### Fixed
- Automatic approval of the first user
- Remove Docker Stack when Environment gets destroyed
- Empty Minio production bucket name

### Removed
- Authentication with Gitlab
- Environment variable based encryption keys


## 2.5.0 - 2019-11-12

### Added
- Custom primary color
- Support for External service
- Added Minio integration
- Added option for different review domain schema

### Changed
- Stack templates


## 2.4.0 - 2019-08-24

### Added
- Docker details auto-reload
- Expose frontend hosts as environment variable (`CARGO_FRONTEND_<NAME>_HOSTS`) to services
- Verify frontend hosts DNS entries
- Restart docker services


## 2.3.0 - 2019-08-15

### Added
- Dropdown to open frontend hosts
- Validation for too long hostnames
- The deployment form is now prefilled with the latest deployment
- Hints to fields which can't be changed later
- Order models by default

### Changed
- Minor layout improvements

### Fixed
- Admin policies


## 2.2.0 - 2019-05-20

### Added
- Settings page for admin users
- Basic user management
- Javascript error logging
- Automatic reload if a service is unavailable

### Changed
- Migrated some configs into the new admin settings page
- Bootstrap custom forms
- Terminal close warning
- Internal error pages
- Stack default production environment name is now configurable
- Migrated to Rails 6


## 2.1.0 - 2019-04-16

### Added
- Optimistic locking to Configs
- Error pages
- Metrics from Traefik
- Prometheus

### Changed
- Improve service template
- Updates dependencies

### Fixed
- Config update


## 2.0.0 - 2019-02-17

### Added
- Cargo configuration class
- Web terminal for accessing a containers shell
- Postgres database
- Redis for caching and websockets

### Changed
- Uses UUIDs instead of integers as ids
- Aborts logs streaming when user visits a new page
- Uses DockerAPI Gem for communication with Docker
- Stack internal networks are encrypted by default

### Removed
- Sqlite3 database


## 1.2.0 - 2019-01-23

### Added
- some basic healthchecks to the rails template
- some default app directories into the cli image
- dummy user in development

### Changed
- Volume claims to not be read-only by default
- Sanitize docker compose

### Fixed
- Frontend review host name
- Setup and update commands for development


## 1.1.1 - 2019-01-07

### Fixed
- Stack members migration


## 1.1.0 - 2019-01-04

### Added
- Access to docker service log stream
- Some Docker stack insights
- Stack Templates
- Cargo CLI
- Code Editor to configs
- Config syntax highlighting
- Autofill for service config target
- Stack production environment name
- Service healthcheck
- Sentry for error monitoring
- docker name to stacks
- `CARGO_ENV` environment variable in all services
- simple access authorization per stack
- separate config value for non production deployments

### Changed
- Updates Dependencies
- Service default deploy options
- makes frontend field `hosts` optional

### Fixed
- Frontend basic auth
- Stacks sorting


## 1.0.0 - 2018-11-23

initial release :tada:
