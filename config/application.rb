# frozen_string_literal: true

require_relative "boot"

require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Cargo
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.0

    config.time_zone = "Europe/Berlin"
    config.i18n.load_path += Dir[Rails.root.join("config/locales/**/*.yml").to_s]

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
    config.eager_load_paths << Rails.root.join("lib")

    require "cargo"

    config.version = Rails.root.join("VERSION").read.squish
    config.revision = Rails.root.join("REVISION").read.squish[0..7] rescue "dev" # rubocop:disable Style/RescueModifier

    config.generators do |g|
      g.orm :active_record, primary_key_type: :uuid
    end
  end
end
