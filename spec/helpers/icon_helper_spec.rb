# frozen_string_literal: true

require "rails_helper"

RSpec.describe IconHelper, type: :helper do
  describe "#embedded_svg" do
    it { expect(helper.embedded_svg("shipment-cargo-boat.svg")).to include("<svg") }
    it { expect(helper.embedded_svg("shipment-cargo-boat.svg", class: :test)).to include(%(class="test")) }
  end

  describe "#icon" do
    it { expect(helper.icon("cargo")).to include("<svg") }
    it { expect(helper.icon("cargo", class: :foobar)).to include("foobar") }
    it { expect(helper.icon("cargo")).to include("#cargo") }
  end
end
