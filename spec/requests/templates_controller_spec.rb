# frozen_string_literal: true

require "rails_helper"

RSpec.describe Templates, type: :request do
  before { allow(User).to receive(:find_by).and_return(user) }

  let(:user) { create(:user) }
  let(:stack) { create(:stack, users: [user]) }

  describe "#new" do
    it "shows the form to import the template" do
      get stack_new_template_path(stack, :rails)

      expect(response).to have_http_status(:success)
      expect(response.body).to include("Import template")
      expect(response.body).to include(%(name="template_service[image]"))
    end
  end

  describe "#create" do
    let(:attributes) { { image: "nginx:latest", application_directory: "/", rails_master_key: "foobar" } }

    it "imports the template" do
      post stack_templates_path(stack), params: { template_service: attributes, name: :rails }

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(%r{stacks/[\w\-]{36}})
    end

    it "renders validation errors" do
      post stack_templates_path(stack), params: { template_service: { image: "" }, name: :rails }

      expect(response).to have_http_status(:success)
      expect(response.body).to include("Please review the problems below:")
      expect(response.body).to include("Docker image can&#39;t be blank")
    end
  end
end
