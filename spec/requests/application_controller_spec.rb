# frozen_string_literal: true

require "rails_helper"

RSpec.describe ApplicationController, type: :request do
  describe "#root" do
    it "redirects to OpenId connect" do
      get root_path

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to("/auth/openid_connect")
    end
  end
end
