# frozen_string_literal: true

require "rails_helper"

RSpec.describe ServicesController, type: :request do
  before { allow(User).to receive(:find_by).and_return(user) }

  let(:user) { create(:user) }
  let(:stack) { create(:stack, users: [user]) }

  describe "#index" do
    it "contains the service" do
      service = create(:service, stack: stack)

      get stack_services_path(stack)

      expect(response).to have_http_status(:success)
      expect(response.body).to include(service.name)
      expect(response.body).to include(service_path(service))
    end

    it "contains the link to create a new service" do
      get stack_services_path(stack)

      expect(response).to have_http_status(:success)
      expect(response.body).to include(new_stack_service_path(stack))
    end
  end

  describe "#show" do
    let(:service) { create(:service, stack: stack) }

    it "shows the service" do
      get service_path(service)

      expect(response).to have_http_status(:success)
      expect(response.body).to include(service.name)
    end

    it "contains the link to edit the service" do
      get service_path(service)

      expect(response).to have_http_status(:success)
      expect(response.body).to include(edit_service_path(service))
    end
  end

  describe "#new" do
    it "shows the form to create a service" do
      get new_stack_service_path(stack)

      expect(response).to have_http_status(:success)
      expect(response.body).to include("New service")
      expect(response.body).to include(%(name="service[name]"))
      expect(response.body).to include("Create Service")
    end
  end

  describe "#create" do
    it "creates a service" do
      expect do
        post stack_services_path(stack), params: { service: { name: "Test", image: "nginx:latest" } }
      end.to change(Service, :count).by(1)

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(%r{services/[\w\-]{36}})
    end

    it "renders validation errors" do
      post stack_services_path(stack), params: { service: { name: "" } }

      expect(response).to have_http_status(:success)
      expect(response.body).to include("Please review the problems below:")
      expect(response.body).to include("Name can&#39;t be blank")
    end
  end

  describe "#edit" do
    let(:service) { create(:service, stack: stack) }

    it "shows the form to create a service" do
      get edit_service_path(service)

      expect(response).to have_http_status(:success)
      expect(response.body).to include("Editing service")
      expect(response.body).to include(%(name="service[name]"))
      expect(response.body).to include("Update Service")
    end
  end

  describe "#update" do
    let(:service) { create(:service, stack: stack) }

    it "updates the service" do
      expect do
        put service_path(service), params: { service: { name: "Test" } }
      end.to change { service.reload.name }.from(service.name).to("Test")

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(service_path(service))
    end

    it "renders validation errors" do
      put service_path(service), params: { service: { name: "" } }

      expect(response).to have_http_status(:success)
      expect(response.body).to include("Please review the problems below:")
      expect(response.body).to include("Name can&#39;t be blank")
    end
  end

  describe "#duplicate" do
    let!(:service) { create(:service, stack: stack) }

    it "duplicates the service" do
      expect do
        post duplicate_service_path(service)
      end.to change(Service, :count).by(1)

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(%r{services/[\w\-]{36}})
    end

    it "redirects to service" do
      expect_any_instance_of(Service).to receive(:save).and_return(false)

      expect do
        post duplicate_service_path(service)
      end.to change(Service, :count).by(0)

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(service_path(service))
    end
  end

  describe "#destroy" do
    let(:service) { create(:service, stack: stack) }

    it "destroys the service" do
      delete service_path(service)

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(stack_services_path(stack))
    end

    it "does not destroy the service" do
      expect_any_instance_of(Service).to receive(:destroy).and_return(false)

      delete service_path(service)

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(service_path(service))
    end
  end
end
