# frozen_string_literal: true

require "rails_helper"

RSpec.describe ErrorsController, type: :request do
  describe "#catch_all" do
    it "renders service unavailable" do
      get "/catch-all", headers: { "X-CATCHALL" => "traefik", "X-FORWARDED-HOST" => Current.settings.host }

      expect(response).to have_http_status(:service_unavailable)
      expect(response.body).to include("Service Unavailable")
    end

    it "renders not found" do
      get "/catch-all", headers: { "X-CATCHALL" => "traefik", "X-FORWARDED-HOST" => "unknown.example.com" }

      expect(response).to have_http_status(:not_found)
      expect(response.body).to include("Not Found")
    end

    it { expect { get "/catch-all" }.to raise_error(ActionController::RoutingError) }
  end

  describe "#service_unavailable" do
    it do
      get service_unavailable_errors_path

      expect(response).to have_http_status(:service_unavailable)
      expect(response.body).to include("Service Unavailable")
    end
  end

  describe "#not_found" do
    it "text/html" do
      get not_found_errors_path

      expect(response).to have_http_status(:not_found)
      expect(response.content_type).to eq("text/html; charset=utf-8")
      expect(response.body).to include("Not Found")
    end

    it "application/json" do
      get not_found_errors_path(format: :json)

      expect(response).to have_http_status(:not_found)
      expect(response.content_type).to eq("application/json; charset=utf-8")
      expect(response.body).to include("Not Found")
    end

    it "text/css" do
      get not_found_errors_path(format: :css)

      expect(response).to have_http_status(:not_found)
      expect(response.content_type).to eq("text/css; charset=utf-8")
      expect(response.body).to include("Not Found")
    end

    it "text/javascript" do
      get not_found_errors_path(format: :js)

      expect(response).to have_http_status(:not_found)
      expect(response.content_type).to eq("text/javascript; charset=utf-8")
      expect(response.body).to include("Not Found")
    end

    it "application/xml" do
      get not_found_errors_path(format: :xml)

      expect(response).to have_http_status(:not_found)
      expect(response.content_type).to eq("application/xml; charset=utf-8")
      expect(response.body).to include("Not Found")
    end

    it "image/svg+xml" do
      get not_found_errors_path(format: :svg)

      expect(response).to have_http_status(:not_found)
      expect(response.content_type).to eq("image/svg+xml; charset=utf-8")
      expect(response.body).to include("Not Found")
    end

    it "unknown format" do
      get not_found_errors_path(format: :php)

      expect(response).to have_http_status(:not_found)
      expect(response.content_type).to eq("text/html; charset=utf-8")
      expect(response.body).to include("Not Found")
    end
  end
end
