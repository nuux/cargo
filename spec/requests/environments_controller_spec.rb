# frozen_string_literal: true

require "rails_helper"

RSpec.describe EnvironmentsController, type: :request do
  before { allow(User).to receive(:find_by).and_return(user) }

  let(:user) { create(:user) }
  let(:stack) { create(:stack, users: [user]) }
  let(:environment) { create(:environment, stack: stack, last_deployment: create(:deployment)) }

  describe "#show" do
    it "shows a environment" do
      get environment_path(environment)

      expect(response).to have_http_status(:success)
      expect(response.body).to include(environment.name)
    end

    it "returns http status success" do
      get environment_path(environment, format: :json)

      expect(response).to have_http_status(:success)
    end
  end

  describe "#new" do
    it "shows the form to create a environment" do
      get new_stack_environment_path(stack)

      expect(response).to have_http_status(:success)
      expect(response.body).to include("New environment")
      expect(response.body).to include(%(name="environment[name]"))
      expect(response.body).to include("Create Environment")
    end
  end

  describe "#create" do
    it "creates a environment" do
      expect do
        post stack_environments_path(stack), params: { environment: { name: "Test" } }
      end.to change(Environment, :count).by(1)

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(stack_deployments_path(stack))
    end

    it "renders validation errors" do
      post stack_environments_path(stack), params: { environment: { name: "" } }

      expect(response).to have_http_status(:success)
      expect(response.body).to include("Please review the problems below:")
      expect(response.body).to include("Name can&#39;t be blank")
    end
  end

  describe "#destroy" do
    let(:environment) { create(:environment, stack: stack) }

    it "destroys the environment" do
      delete environment_path(environment)

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(stack_path(stack))
    end
  end
end
