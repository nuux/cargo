# frozen_string_literal: true

require "rails_helper"

RSpec.describe FrontendsController, type: :request do
  before { allow(User).to receive(:find_by).and_return(user) }

  let(:user) { create(:user) }
  let(:stack) { create(:stack, users: [user]) }
  let(:service) { create(:service, stack: stack) }

  describe "#index" do
    it "contains the frontend" do
      frontend = create(:frontend, service: service)

      get stack_frontends_path(stack)

      expect(response).to have_http_status(:success)
      expect(response.body).to include(frontend.name)
      expect(response.body).to include(frontend_path(frontend))
    end

    it "contains the link to create a new frontend" do
      get stack_frontends_path(stack)

      expect(response).to have_http_status(:success)
      expect(response.body).to include(new_stack_frontend_path(stack))
    end
  end

  describe "#show" do
    let(:frontend) { create(:frontend, service: service) }

    it "shows the frontend" do
      get frontend_path(frontend)

      expect(response).to have_http_status(:success)
      expect(response.body).to include(frontend.name)
    end

    it "contains the link to edit the frontend" do
      get frontend_path(frontend)

      expect(response).to have_http_status(:success)
      expect(response.body).to include(edit_frontend_path(frontend))
    end
  end

  describe "#new" do
    it "shows the form to create a frontend" do
      get new_stack_frontend_path(stack)

      expect(response).to have_http_status(:success)
      expect(response.body).to include("New frontend")
      expect(response.body).to include(%(name="frontend[name]"))
      expect(response.body).to include("Create Frontend")
    end
  end

  describe "#create" do
    it "creates a frontend" do
      expect do
        post stack_frontends_path(stack), params: { frontend: { name: "Test", service_id: service.id, hosts: "https://test.example.com", port: 3000, ignore_wrong_dns_entries: "1" } }
      end.to change(Frontend, :count).by(1)

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(%r{frontends/[\w\-]{36}})
    end

    it "renders validation errors" do
      post stack_frontends_path(stack), params: { frontend: { name: "" } }

      expect(response).to have_http_status(:success)
      expect(response.body).to include("Please review the problems below:")
      expect(response.body).to include("Name can&#39;t be blank")
    end
  end

  describe "#edit" do
    let(:frontend) { create(:frontend, service: service) }

    it "shows the form to create a frontend" do
      get edit_frontend_path(frontend)

      expect(response).to have_http_status(:success)
      expect(response.body).to include("Editing frontend")
      expect(response.body).to include(%(name="frontend[name]"))
      expect(response.body).to include("Update Frontend")
    end
  end

  describe "#update" do
    let(:frontend) { create(:frontend, service: service) }

    it "updates the frontend" do
      expect do
        put frontend_path(frontend), params: { frontend: { hosts: "https://example.com", ignore_wrong_dns_entries: "1" } }
      end.to change { frontend.reload.hosts }.from(frontend.hosts).to("https://example.com")

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(frontend_path(frontend))
    end

    it "renders validation errors" do
      put frontend_path(frontend), params: { frontend: { name: "" } }

      expect(response).to have_http_status(:success)
      expect(response.body).to include("Please review the problems below:")
      expect(response.body).to include("Name can&#39;t be blank")
    end
  end

  describe "#destroy" do
    let(:frontend) { create(:frontend, service: service) }

    it "destroys the frontend" do
      delete frontend_path(frontend)

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(stack_frontends_path(stack))
    end

    it "does not destroy the frontend" do
      expect_any_instance_of(Frontend).to receive(:destroy).and_return(false)

      delete frontend_path(frontend)

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(frontend_path(frontend))
    end
  end
end
