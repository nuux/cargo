# frozen_string_literal: true

require "rails_helper"

RSpec.describe MetricsController, type: :request do
  before { allow(User).to receive(:find_by).and_return(user) }

  let(:user) { create(:user) }
  let(:stack) { create(:stack, users: [user]) }
  let(:service) { create(:service, stack: stack) }
  let(:environment) { create(:environment, stack: stack) }

  describe "#requests" do
    it do
      create(:application_setting, prometheus: true, prometheus_host: "foobar")
      expect_any_instance_of(MetricsService).to receive(:requests).and_return({})

      get requests_environment_service_metrics_path(environment, service)

      expect(response).to have_http_status(:success)
      expect(response.body).to eq("{}")
    end
  end

  describe "#response_time" do
    it do
      create(:application_setting, prometheus: true, prometheus_host: "foobar")
      expect_any_instance_of(MetricsService).to receive(:response_time).and_return({})

      get response_time_environment_service_metrics_path(environment, service)

      expect(response).to have_http_status(:success)
      expect(response.body).to eq("{}")
    end
  end
end
