# frozen_string_literal: true

require "rails_helper"

RSpec.describe UsersController, type: :request do
  before { allow(User).to receive(:find_by).and_return(current_user) }

  let(:current_user) { create(:admin) }

  describe "#index" do
    it "contains the user" do
      user = create(:user)

      get users_path

      expect(response).to have_http_status(:success)
      expect(response.body).to include(current_user.name)
      expect(response.body).to include(user.name)
    end

    context "when current_user is no admin" do
      let(:current_user) { create(:user) }

      it "redirects to root_path" do
        get users_path

        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
        expect(flash[:alert]).to include("not authorized")
      end
    end
  end

  describe "#update" do
    let(:user) { create(:user, approved: true) }

    it "updates the user" do
      expect do
        put user_path(user), params: { user: { approved: "0" } }
      end.to change { user.reload.approved }.from(true).to(false)

      expect(response).to have_http_status(:ok)
      expect(response.body).to be_empty
    end

    it "renders validation errors" do
      put user_path(user), params: { user: { approved: "" } }

      expect(response).to have_http_status(:unprocessable_entity)
      expect(response.body).to include("alert")
      expect(response.body).to include("the user could not be saved")
    end
  end

  describe "#destroy" do
    let(:user) { create(:user) }

    it "destroys the user" do
      delete user_path(user)

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(users_path)
      expect(flash[:alert]).to be_blank
      expect(flash[:success]).to include("deleted")
    end

    it "does not destroy the user" do
      expect_any_instance_of(User).to receive(:destroy).and_return(false)

      delete user_path(user)

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(users_path)
      expect(flash[:alert]).to include("not deleted")
    end
  end
end
