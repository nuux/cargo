# frozen_string_literal: true

require "rails_helper"

RSpec.describe Docker::LogsController, type: :request do
  before { allow(User).to receive(:find_by).and_return(user) }

  let(:user) { create(:user) }
  let(:stack) { create(:stack, users: [user]) }
  let(:environment) { create(:environment, stack: stack) }

  describe "#index" do
    it "shows logs" do
      get environment_service_logs_path(environment, "304waajn57x2ddae73hlnpt6u")

      expect(response).to have_http_status(:success)
      expect(response.body).to include(environment.name)
      expect(response.body).to include("Log stream")
    end
  end

  describe "#stream" do
    before { expect_any_instance_of(LogsStreamService).to receive(:close).at_least(:once).and_call_original }

    it "streams logs" do
      get stream_environment_service_logs_path(environment, "304waajn57x2ddae73hlnpt6u", format: :txt)

      expect(response).to have_http_status(:success)
      expect(response.headers["Content-Type"]).to eq("text/event-stream")
      expect(response.body).to include(%(10.0.2.3 - - [03/Feb/2019:17:50:51 +0000] "GET / HTTP/1.1" 200 612 "-" "Wget" "-"))
    end

    it "handles errors" do
      expect_any_instance_of(LogsStreamService).to receive(:run).and_raise(ActionController::Live::ClientDisconnected)

      get stream_environment_service_logs_path(environment, "304waajn57x2ddae73hlnpt6u", format: :txt)

      expect(response).to have_http_status(:success)
    end
  end
end
