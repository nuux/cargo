# frozen_string_literal: true

require "rspec/expectations"

RSpec::Matchers.define :be_hash_with do |expected_key, expected_value|
  match do |actual|
    def parse_wildcards(string)
      matching_parts = string.split("*", -1).map { |part| Regexp.escape(part) }
      /\A#{matching_parts.join(".*")}\z/i
    end

    expected_key_regexp = parse_wildcards expected_key.to_s
    expected_value_regexp = parse_wildcards expected_value.to_s

    key = actual.keys.find { |k| k.to_s.match?(expected_key_regexp) }

    next false if key.blank?

    actual[key] == expected_value || actual[key].to_s.match?(expected_value_regexp)
  end
end
