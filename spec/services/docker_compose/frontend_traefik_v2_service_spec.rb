# frozen_string_literal: true

require "rails_helper"

RSpec.describe DockerCompose::FrontendTraefikV2Service do
  subject(:service) { described_class.new(frontend: frontend, deployment: deployment) }

  let(:frontend) { create(:frontend) }
  let(:environment) { create(:environment) }
  let(:deployment) { create(:deployment, environment: environment) }

  describe "#labels" do
    let(:result) { service.labels }

    describe "default" do
      it { expect(result).to be_hash_with("traefik.enable", "true") }
      it { expect(result).to be_hash_with("traefik.http.routers.*.rule", "Host(`*.localhost`)") }
      it { expect(result).to be_hash_with("traefik.http.services.*.loadbalancer.server.port", frontend.port) }

      context "when production" do
        let(:environment) { build(:environment, name: "production") }
        let(:frontend) { create(:frontend, hosts: "production.example.com") }

        it { expect(result).to be_hash_with("traefik.http.routers.*.rule", "Host(`production.example.com`)") }

        context "when host is invalid" do
          let(:frontend) { create(:frontend, hosts: "invalid_host.example.com") }

          it { expect { result }.to raise_error(DockerComposeService::Error, %(The hostname "invalid_host.example.com" is invalid)) }
        end

        context "when host is too long" do
          let(:frontend) { create(:frontend, hosts: "123456789012345678901234567890123456789012345678901234567890.example.com") }

          it { expect { result }.to raise_error(DockerComposeService::Error, %(The hostname "123456789012345678901234567890123456789012345678901234567890.example.com" is too long (max. 64 characters))) }
        end

        context "when router rule is defined" do
          let(:frontend) { create(:frontend, hosts: "production.example.com", router_rule: "Host(`custom`)") }

          it { expect(result).to be_hash_with("traefik.http.routers.*.rule", "Host(`custom`)") }
        end

        context "when router and host is missing" do
          let(:frontend) { create(:frontend, hosts: nil, router_rule: nil) }

          it { expect(result).to be_hash_with("traefik.http.routers.*.rule", "Host(`#{deployment.stack.short_name}-#{frontend.name}-#{deployment.name}.localhost`)") }
        end
      end

      context "when review" do
        it {  expect(result).to be_hash_with("traefik.http.routers.*.rule", "Host(`#{deployment.stack.short_name}-#{frontend.name}-#{deployment.name}.localhost`)") }
      end
    end

    describe "tls" do
      before do
        Current.settings.traefik_cert_resolvers = "acme-test-tls"
      end

      let(:frontend) { create(:frontend, hosts: "production.example.com", tls: true, tls_cert_resolver: "acme-test-tls", tls_domains: "main.example.org,sans.example.org", tls_options: "modern") }

      it { expect(result).not_to be_hash_with("traefik.http.routers.*.tls", "*") }

      context "when production" do
        let(:environment) { build(:environment, name: "production") }

        it { expect(result).to be_hash_with("traefik.http.routers.*.tls", "true") }
        it { expect(result).to be_hash_with("traefik.http.routers.*.tls.certresolver", "acme-test-tls") }
        it { expect(result).to be_hash_with("traefik.http.routers.*.tls.domains[0].main", "main.example.org") }
        it { expect(result).to be_hash_with("traefik.http.routers.*.tls.domains[0].sans", "sans.example.org") }
        it { expect(result).to be_hash_with("traefik.http.routers.*.tls.options", "modern@file") }
      end
    end

    describe "auth_basic" do
      let(:frontend) { create(:frontend, auth_basic: true, auth_basic_users: "test: test") }

      it { expect(result).to be_hash_with("traefik.enable", "true") }
      it { expect(result).to be_hash_with("traefik.http.middlewares.*-baiscauth.basicauth.users", "test:*") }
      it { expect(result).to be_hash_with("traefik.http.middlewares.*-baiscauth.basicauth.removeheader", "true") }

      it "catches exceptions" do
        allow(BCrypt::Password).to receive(:create).and_raise(StandardError)

        expect(result).to be_hash_with("traefik.http.middlewares.*-baiscauth.basicauth.users", "")
      end
    end

    describe "redirect" do
      let(:frontend) { create(:frontend, redirect: true, redirect_regex: "^https://www\\.(.*)", redirect_replacement: "https://$$1") }

      it { expect(result).to be_hash_with("traefik.enable", "true") }
      it { expect(result).to be_hash_with("traefik.http.middlewares.*-redirect.redirectregex.regex", "^https://www\\.(.*)") }
      it { expect(result).to be_hash_with("traefik.http.middlewares.*-redirect.redirectregex.replacement", "https://$$1") }
      it { expect(result).to be_hash_with("traefik.http.middlewares.*-redirect.redirectregex.permanent", "true") }
    end

    describe "custom_headers" do
      let(:frontend) { create(:frontend, request_headers: "a: b", response_headers: "c: d\ne: f") }

      it { expect(result).to be_hash_with("traefik.enable", "true") }
      it { expect(result).to be_hash_with("traefik.http.middlewares.*-headers.headers.customRequestHeaders.a", "b") }
      it { expect(result).to be_hash_with("traefik.http.middlewares.*-headers.headers.customResponseHeaders.c", "d") }
      it { expect(result).to be_hash_with("traefik.http.middlewares.*-headers.headers.customResponseHeaders.e", "f") }

      it "catches exceptions" do
        allow(YAML).to receive(:safe_load).exactly(4).times.and_raise(Psych::SyntaxError)

        expect(result).not_to be_hash_with("traefik.http.middlewares.*-headers.headers.customRequestHeaders.*", "*")
        expect(result).not_to be_hash_with("traefik.http.middlewares.*-headers.headers.customResponseHeaders.*", "*")
      end
    end

    describe "hsts" do
      let(:environment) { build(:environment, name: "production") }
      let(:frontend) { create(:frontend, hsts: true, response_headers: "c: d\ne: f") }

      it { expect(result).to be_hash_with("traefik.enable", "true") }
      it { expect(result).to be_hash_with("traefik.http.middlewares.*-hsts.headers.forceSTSHeader", "true") }
      it { expect(result).to be_hash_with("traefik.http.middlewares.*-hsts.headers.stsSeconds", "15552000") }
      it { expect(result).to be_hash_with("traefik.http.middlewares.*-hsts.headers.stsIncludeSubdomains", "false") }
    end
  end

  describe "#environment" do
    it { expect(service.environment).to be_hash_with("CARGO_FRONTEND_*_RULE", "Host(`*.localhost`)") }
  end
end
