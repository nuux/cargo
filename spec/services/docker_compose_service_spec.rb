# frozen_string_literal: true

require "rails_helper"

RSpec.describe DockerComposeService do
  subject { described_class.new(deployment: deployment) }

  let!(:stack) { create(:stack, name: "stackname", short_name: "s") }
  let!(:service) { create(:service, stack: stack, name: "servicename") }
  let!(:config) { create(:config_claim, service: service, target: "config", config: create(:config, name: "configname", stack: stack, value: "value", review_value: "review_value")) }
  let!(:config_environment) { create(:config_claim, service: service, target: "environment", config: create(:config_environment, name: "environmentname", stack: stack, value: "value", review_value: "review_value")) }
  let!(:config_secret) { create(:config_claim, service: service, target: "secret", config: create(:config_secret, name: "secretname", stack: stack, value: "value", review_value: "review_value")) }
  let!(:volume) { create(:volume_claim, service: service, target: "volume", volume: create(:volume, name: "volumename")) }
  let!(:frontend) { create(:frontend, service: service, name: "frontendname", port: 3000) }
  let!(:environment) { create(:environment, stack: stack, name: "environmentname") }
  let!(:deployment) { create(:deployment, stack: stack, environment: environment) }

  describe "#content" do
    let(:content) { subject.content }

    context "when traefik version is v1" do
      before { allow(Current.settings).to receive(:traefik_version).and_return("v1") }

      let(:example) do
        yaml = <<~YAML
          ---
          version: '3.7'
          services:
            servicename:
              image: servicename:latest
              volumes:
              - volumename:volume
              networks:
              - overlay
              - traefik
              configs:
              - source: cargo_s_configname_review_0
                target: config
              environment:
                CARGO_ENV: environmentname
                environment: review_value
                CARGO_FRONTEND_FRONTENDNAME_HOSTS: s-frontendname-environmentname.localhost
              secrets:
              - source: cargo_s_secretname_review_0
                target: secret
              deploy:
                replicas: 1
                rollback_config:
                  parallelism: 1
                  delay: 60s
                  failure_action: continue
                  monitor: 60s
                  order: start-first
                update_config:
                  parallelism: 1
                  delay: 60s
                  failure_action: rollback
                  monitor: 60s
                  order: start-first
                restart_policy:
                  condition: any
                  delay: 60s
                  window: 60s
                labels:
                  traefik.enable: 'true'
                  traefik.frontendname.port: 3000
                  traefik.frontendname.frontend.rule: Host:s-frontendname-environmentname.localhost
                  traefik.frontendname.frontend.passHostHeader: 'true'
                  traefik.frontendname.frontend.errors.cargo.backend: cargo-cargo-web
                  traefik.frontendname.frontend.errors.cargo.status: '502,503,504'
                  traefik.frontendname.frontend.errors.cargo.query: "/errors/service_unavailable"
          configs:
            cargo_s_configname_review_0:
              external: true
          secrets:
            cargo_s_secretname_review_0:
              external: true
          volumes:
            volumename:
          networks:
            traefik:
              external: true
            overlay:
              driver: overlay
              driver_opts:
                encrypted: 'true'

        YAML
        YAML.safe_load(yaml).to_yaml
      end

      it { expect(content).to be_a(Hash) }

      it "matches the example" do
        expect(content.to_yaml).to eq(example)
      end
    end

    context "when traefik version is v2" do
      before { allow(Current.settings).to receive(:traefik_version).and_return("v2") }

      let(:example) do
        yaml = <<~YAML
          ---
          version: '3.7'
          services:
            servicename:
              image: servicename:latest
              volumes:
              - volumename:volume
              networks:
              - overlay
              - traefik
              configs:
              - source: cargo_s_configname_review_0
                target: config
              environment:
                CARGO_ENV: environmentname
                environment: review_value
                CARGO_FRONTEND_FRONTENDNAME_RULE: Host(`s-frontendname-environmentname.localhost`)
              secrets:
              - source: cargo_s_secretname_review_0
                target: secret
              deploy:
                replicas: 1
                rollback_config:
                  parallelism: 1
                  delay: 60s
                  failure_action: continue
                  monitor: 60s
                  order: start-first
                update_config:
                  parallelism: 1
                  delay: 60s
                  failure_action: rollback
                  monitor: 60s
                  order: start-first
                restart_policy:
                  condition: any
                  delay: 60s
                  window: 60s
                labels:
                  traefik.enable: 'true'
                  traefik.http.services.cargo-s-environmentname-servicename-frontendname.loadbalancer.server.port: 3000
                  traefik.http.services.cargo-s-environmentname-servicename-frontendname.loadbalancer.passhostheader: 'true'
                  traefik.http.routers.cargo-s-environmentname-servicename-frontendname.rule: Host(`s-frontendname-environmentname.localhost`)
                  traefik.http.routers.cargo-s-environmentname-servicename-frontendname.service: cargo-s-environmentname-servicename-frontendname
                  traefik.http.routers.cargo-s-environmentname-servicename-frontendname.middlewares: cargo-s-environmentname-servicename-frontendname-errors
                  traefik.http.middlewares.cargo-s-environmentname-servicename-frontendname-errors.errors.service: cargo-cargo-web
                  traefik.http.middlewares.cargo-s-environmentname-servicename-frontendname-errors.errors.status: '502,503,504'
                  traefik.http.middlewares.cargo-s-environmentname-servicename-frontendname-errors.errors.query: "/errors/service_unavailable"
          configs:
            cargo_s_configname_review_0:
              external: true
          secrets:
            cargo_s_secretname_review_0:
              external: true
          volumes:
            volumename:
          networks:
            traefik:
              external: true
            overlay:
              driver: overlay
              driver_opts:
                encrypted: 'true'

        YAML
        YAML.safe_load(yaml).to_yaml
      end

      it { expect(content).to be_a(Hash) }

      it "matches the example" do
        expect(content.to_yaml).to eq(example)
      end
    end
  end
end
