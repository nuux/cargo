# frozen_string_literal: true

require "rails_helper"

RSpec.describe TemplateService, type: :model do
  subject(:template) { described_class.new(name: "rails", stack: stack) }

  let(:stack) { create(:stack) }

  it { is_expected.to respond_to(:name) }

  it { is_expected.to respond_to(:image) }
  it { is_expected.to respond_to(:image=) }
  it { is_expected.not_to respond_to(:foobar) }

  describe ".all" do
    it { expect(described_class.all).to be_kind_of(Array) }
    it { expect(described_class.all.first).to be_instance_of(described_class) }
  end

  describe "#inputs" do
    it { expect(template.inputs.keys.sort).to eq(%w[application_directory image rails_master_key]) }
  end

  describe "#perform" do
    let(:attributes) { { image: "nginx:latest", application_directory: "/", rails_master_key: "foobar" } }

    it { expect(template.perform).to be_falsey }
    it { expect(template.perform(attributes)).to be_truthy }

    it do
      expect_any_instance_of(Template::ImportService).to receive(:import).and_return(false)

      expect(template.perform(attributes)).to be_falsey
      expect(template.errors.keys).to include(:base)
    end
  end

  describe "#foobar" do
    it { expect { template.foobar }.to raise_error(NoMethodError) }
  end
end
