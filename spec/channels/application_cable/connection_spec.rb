# frozen_string_literal: true

require "rails_helper"

RSpec.describe ApplicationCable::Connection, type: :channel do
  let(:user) { create(:user) }

  it "successfully connects" do
    connect "/cable", session: { current_user_id: user.id }
    expect(connection.current_user).to eq user
  end

  it "rejects connection" do
    expect { connect "/cable" }.to have_rejected_connection
  end
end
