# frozen_string_literal: true

require "rails_helper"

RSpec.describe User, type: :model do
  subject { build(:user) }

  it { is_expected.to have_many(:stack_members).dependent(:destroy) }
  it { is_expected.to have_many(:stacks).through(:stack_members) }

  it { is_expected.to have_many(:services).through(:stacks) }
  it { is_expected.to have_many(:volumes).through(:stacks) }
  it { is_expected.to have_many(:frontends).through(:stacks) }
  it { is_expected.to have_many(:configs).through(:stacks) }
  it { is_expected.to have_many(:environments).through(:stacks) }
  it { is_expected.to have_many(:deployments).through(:stacks) }

  it { is_expected.to validate_presence_of(:username) }
  it { is_expected.to validate_uniqueness_of(:username).case_insensitive }
  it { is_expected.to have_db_index(:username).unique(true) }

  it { is_expected.to validate_presence_of(:oidc_uid) }
  it { is_expected.to validate_uniqueness_of(:oidc_uid).case_insensitive }
  it { is_expected.to have_db_index(:oidc_uid).unique(true) }

  it { is_expected.to validate_presence_of(:oidc_token) }
  it { is_expected.to validate_uniqueness_of(:oidc_token).case_insensitive }
  it { is_expected.to have_db_index(:oidc_token).unique(true) }

  describe ".from_omniauth" do
    let(:auth_hash) { { uid: 1, info: { nickname: "test" }, credentials: { token: "token" } }.deep_stringify_keys }

    context "when user" do
      let(:user) { described_class.from_omniauth(auth_hash) }

      it { expect(user).to be_a(described_class) }
      it { expect(user.username).to eq("test") }
      it { expect(user.oidc_uid).to eq("1") }
      it { expect(user.oidc_token).to eq("token") }

      context "when user is the first user" do
        it { expect(described_class.count).to eq(0) }
        it { expect(user.admin).to eq(true) }
        it { expect(user.approved).to eq(true) }
      end

      context "when user is the second user" do
        before { create(:user) }

        it { expect(described_class.count).to be_positive }
        it { expect(user.admin).to eq(false) }
        it { expect(user.approved).to eq(false) }
      end
    end
  end
end
