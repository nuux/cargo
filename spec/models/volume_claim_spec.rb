# frozen_string_literal: true

require "rails_helper"

RSpec.describe VolumeClaim, type: :model do
  subject(:volume_claim) { build(:volume_claim) }

  it { is_expected.to belong_to(:service).required }
  it { is_expected.to belong_to(:volume).required }

  it { is_expected.to validate_uniqueness_of(:volume_id).case_insensitive.scoped_to(:service_id) }
  it { is_expected.to validate_presence_of(:target) }
  it { is_expected.to validate_uniqueness_of(:target).case_insensitive.scoped_to(:service_id) }

  describe "#stack" do
    context "when volume is nil" do
      subject(:volume_claim) { build(:volume_claim, volume: nil) }

      it { expect(volume_claim.stack).to be(volume_claim.service.stack) }
    end

    context "when service is nil" do
      subject(:volume_claim) { build(:volume_claim, service: nil) }

      it { expect(volume_claim.stack).to be(volume_claim.volume.stack) }
    end
  end
end
