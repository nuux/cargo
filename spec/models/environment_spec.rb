# frozen_string_literal: true

require "rails_helper"

RSpec.describe Environment, type: :model do
  subject(:environment) { build(:environment) }

  it { is_expected.to have_readonly_attribute(:name) }

  it { is_expected.to belong_to(:stack) }
  it { is_expected.to belong_to(:last_deployment).class_name("Deployment").optional }

  it { is_expected.to have_many(:deployments).dependent(:destroy) }
  it { is_expected.to have_many(:external_service_environment_claims).dependent(:destroy) }

  it { is_expected.to validate_presence_of(:stack) }

  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_uniqueness_of(:name).case_insensitive.scoped_to(%i[stack_id deleted_at]) }
  it { is_expected.to have_db_index(%i[stack_id name deleted_at]).unique(true) }
  it { is_expected.to allow_value("a-B-c").for(:name) }
  it { is_expected.not_to allow_value("a_B_c").for(:name) }
  it { is_expected.not_to allow_value("@").for(:name) }

  # TODO
  # scope :not_deleted, -> { where(deleted_at: nil) }

  describe "#production?" do
    it { expect(environment).not_to be_production }

    context "when production" do
      subject(:environment) { build(:environment, name: "production") }

      it { expect(environment).to be_production }
    end

    context "when non production" do
      subject(:environment) { build(:environment, name: "master", stack: build(:stack, production_environment_name: "master")) }

      it { expect(environment).to be_production }
    end
  end

  describe "#docker_name" do
    it { expect(environment.docker_name).to eq("#{environment.stack.docker_name}_#{environment.name.downcase}") }
  end

  describe "#remove_stack" do
    before { expect(DockerCli::Stack).to receive(:remove).with(name: environment.docker_name) }

    it do
      expect(environment.deleted_at).to be_nil
      expect(environment.remove_stack).to be_truthy
      expect(environment.deleted_at).not_to be_nil
    end

    it do
      expect(environment).to receive(:update!).and_raise(StandardError)
      expect(environment.remove_stack).to be_falsey
    end
  end

  describe "#docker_services" do
    before { expect_any_instance_of(DockerApi::Stack).to receive(:services).and_return([]) }

    it { expect(environment.docker_services).to be_kind_of(Array) }
  end

  describe "#docker_tasks" do
    before { expect_any_instance_of(DockerApi::Stack).to receive(:tasks).and_return([]) }

    it { expect(environment.docker_tasks).to be_kind_of(Array) }
  end

  describe "#docker_containers" do
    before { expect_any_instance_of(DockerApi::Stack).to receive(:containers).and_return([]) }

    it { expect(environment.docker_containers).to be_kind_of(Array) }
  end

  describe "#validate_node_label" do
    before { Current.settings.node_labels = ["production"] }

    it { is_expected.to allow_value("production").for(:node_label) }
    it { is_expected.to allow_value(nil).for(:node_label) }
    it { is_expected.not_to allow_value("something").for(:node_label) }
  end
end
