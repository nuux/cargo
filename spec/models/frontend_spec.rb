# frozen_string_literal: true

require "rails_helper"

RSpec.describe Frontend, type: :model do
  subject(:frontend) { create(:frontend) }

  it { expect(described_class).to be_attr_encrypted(:auth_basic_users) }

  # it { is_expected.to belong_to(:stack).required }
  it { is_expected.to belong_to(:service).required }

  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_uniqueness_of(:name).case_insensitive.scoped_to(:stack_id) }
  it { is_expected.to validate_exclusion_of(:name).in_array(%w[docker domain enable port tags protocol weight backend frontend]) }
  it { is_expected.to have_db_index(%i[stack_id name]).unique(true) }
  it { is_expected.to allow_value("a-B-c").for(:name) }
  it { is_expected.not_to allow_value("a_B_c").for(:name) }
  it { is_expected.not_to allow_value("@").for(:name) }

  it { is_expected.to validate_presence_of(:port) }
  it { is_expected.to validate_numericality_of(:port).only_integer.is_greater_than_or_equal_to(0).is_less_than_or_equal_to(65_535) }
  it { is_expected.not_to validate_presence_of(:hosts) }

  context "when redirect is false" do
    subject(:frontend) { build(:frontend, redirect: false) }

    it { is_expected.not_to validate_presence_of(:redirect_regex) }
    it { is_expected.not_to validate_presence_of(:redirect_replacement) }
  end

  context "when redirect is true" do
    subject(:frontend) { build(:frontend, redirect: true) }

    it { is_expected.to validate_presence_of(:redirect_regex) }
    it { is_expected.to validate_presence_of(:redirect_replacement) }
  end

  context "when auth_basic is false" do
    subject(:frontend) { build(:frontend, auth_basic: false) }

    it { is_expected.not_to validate_presence_of(:auth_basic_users) }
  end

  context "when auth_basic is true" do
    subject(:frontend) { build(:frontend, auth_basic: true) }

    it { is_expected.to validate_presence_of(:auth_basic_users) }
  end

  context "when hsts is false" do
    subject(:frontend) { build(:frontend, hsts: false) }

    it { is_expected.not_to validate_numericality_of(:hsts_max_age) }
  end

  context "when hsts is true" do
    subject(:frontend) { build(:frontend, hsts: true) }

    it { is_expected.to validate_numericality_of(:hsts_max_age).only_integer.is_greater_than_or_equal_to(0) }
  end

  it { is_expected.to delegate_method(:name).to(:service).with_prefix }

  describe "#duplicate" do
    let(:duplicate) { frontend.duplicate }

    it { expect(duplicate).to be_a(described_class) }
    it { expect(duplicate.name).to eq("#{frontend.name}-clone") }
    it { expect(duplicate).to be_valid }
  end

  describe "set stack" do
    subject(:frontend) { create(:frontend, stack: nil) }

    it { expect(frontend.stack).to be(frontend.service.stack) }
  end

  describe "#tls_domains=" do
    it do
      frontend.tls_domains = "d1,d2\nd3"
      expect(frontend.tls_domains).to eq(%w[d1 d2 d3])
    end
  end

  describe "#tls_domains" do
    it do
      frontend.tls_domains = nil
      expect(frontend.tls_domains).to eq([])
    end
  end
end
