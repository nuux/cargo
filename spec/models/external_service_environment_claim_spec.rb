# frozen_string_literal: true

require "rails_helper"

RSpec.describe ExternalServiceEnvironmentClaim, type: :model do
  subject(:claim) { build(:external_service_environment_claim) }

  it { expect(described_class).to be_attr_encrypted(:settings) }

  # it { is_expected.to belong_to(:external_service).optional }
  it { is_expected.to belong_to(:environment).required }

  it { is_expected.to validate_presence_of(:external_service) }
  it { is_expected.to validate_uniqueness_of(:external_service_id).case_insensitive.scoped_to(:environment_id) }
  it { is_expected.to validate_uniqueness_of(:environment_id).case_insensitive.scoped_to(:external_service_id) }

  describe "#minio?" do
    context "when setting minio is false" do
      before { allow(Current.settings).to receive(:minio?).and_return(false) }

      it { expect(claim).not_to be_minio }
    end

    context "when setting minio is true" do
      subject(:claim) { build(:external_service_environment_claim, external_service: external_service, settings: { minio: false }) }

      before { allow(Current.settings).to receive(:minio?).and_return(true) }

      context "when external_service.minio is true" do
        let(:external_service) { build(:external_service, service_type: "minio") }

        it { expect(claim).to be_minio }
      end

      context "when external_service.minio is false" do
        let(:external_service) { build(:external_service, service_type: nil) }

        it { expect(claim).not_to be_minio }
      end
    end
  end

  describe "#create_minio_bucket" do
    before { allow(ExternalServices::MinioService).to receive(:create_bucket).with(bucket_name).and_return(mocked: true) }

    let(:bucket_name) { "#{claim.environment.docker_name}_#{claim.external_service.name}".dasherize.downcase }

    it { expect(claim.send(:create_minio_bucket)).to eq(mocked: true, minio: true) }
  end

  describe "#remove_minio_bucket" do
    subject(:claim) { build(:external_service_environment_claim, settings: settings) }

    let(:settings) { { mocked: true, access_key: :mocked, bucket_name: :mocked, policy_name: :mocked } }

    it do
      expect(ExternalServices::MinioService).to receive(:remove_bucket).with(access_key: :mocked, bucket_name: :mocked, policy_name: :mocked)

      claim.send(:remove_minio_bucket)
    end
  end
end
