# frozen_string_literal: true

require "rails_helper"

RSpec.describe ApplicationSetting, type: :model do
  subject(:setting) { build(:application_setting) }

  it { is_expected.to define_enum_for(:frontend_host_schema).with_values(%i[multiple_subdomains single_subdomain]) }
  it { is_expected.to define_enum_for(:traefik_version).with_values(v1: 10, v2: 20) }

  it { expect(described_class).to be_attr_encrypted(:prometheus_host) }
  it { expect(described_class).to be_attr_encrypted(:minio_access_key) }
  it { expect(described_class).to be_attr_encrypted(:minio_secret_key) }

  it { is_expected.to validate_presence_of(:default_production_environment_name) }
  it { is_expected.to allow_value("a-B-c").for(:default_production_environment_name) }
  it { is_expected.not_to allow_value("a_B_c").for(:default_production_environment_name) }

  it { is_expected.to validate_presence_of(:host) }
  it { is_expected.to validate_presence_of(:frontend_host_schema) }
  it { is_expected.to validate_presence_of(:traefik_network) }

  it { is_expected.to allow_value(described_class::TABLE_OPTIONS).for(:table_options) }
  it { is_expected.not_to allow_value(["test"]).for(:table_options) }

  context "when prometheus is false" do
    subject { build(:application_setting, prometheus: false) }

    it { is_expected.not_to validate_presence_of(:prometheus_host) }
  end

  context "when prometheus is true" do
    subject { build(:application_setting, prometheus: true) }

    it { is_expected.to validate_presence_of(:prometheus_host) }
  end

  context "when minio is false" do
    subject { build(:application_setting, minio: false) }

    it { is_expected.not_to validate_presence_of(:minio_host) }
    it { is_expected.not_to validate_presence_of(:minio_access_key) }
    it { is_expected.not_to validate_presence_of(:minio_secret_key) }

    it { is_expected.not_to be_external_services }
  end

  context "when minio is true" do
    subject { build(:application_setting, minio: true) }

    it { is_expected.to validate_presence_of(:minio_host) }
    it { is_expected.to validate_presence_of(:minio_access_key) }
    it { is_expected.to validate_presence_of(:minio_secret_key) }

    it { is_expected.to be_external_services }
  end

  context "when setting is new and empty" do
    subject(:setting) { described_class.new }

    it { expect(setting.primary_color).to eq("#009cff") }
    it { expect(setting.traefik_version).to eq("v2") }
    it { expect(setting.frontend_host_schema).to eq("single_subdomain") }
  end

  describe ".get" do
    it "finds the existing" do
      setting = create(:application_setting)
      expect(described_class.get).to eq(setting)
    end

    it "creates an instance" do
      expect(described_class.count).to be_zero
      expect(described_class.get).to be_instance_of(described_class)
    end
  end

  describe "#user_default_approved" do
    subject(:setting) { build(:application_setting, user_default_approved_regex: "@example\.com$") }

    it { expect(setting.user_default_approved("invalid-email")).to be_falsey }
    it { expect(setting.user_default_approved("email@test.example.com")).to be_falsey }
    it { expect(setting.user_default_approved("email@example.com")).to be_truthy }
  end

  describe "#primar_color" do
    subject(:setting) { build(:application_setting, primary_color: "#009cff") }

    it { expect(setting.primary_color).to eq("#009cff") }
    it { expect(setting.primary_color_hsl).to eq("hsl(203.29, 100.00%, 50.00%)") }
    it { expect(setting.primary_color_h).to eq("203.29") }
    it { expect(setting.primary_color_s).to eq("100.00%") }
    it { expect(setting.primary_color_l).to eq("50.00%") }
    it { expect(setting.style).to include("--primary-color") }
  end

  describe "#validate_minio_connection" do
    subject(:setting) { build(:application_setting, minio: true, minio_host: :mocked, minio_access_key: :mocked, minio_secret_key: :mocked) }

    it do
      expect_any_instance_of(ExternalServices::MinioService).to receive(:try_connection).and_call_original # rubocop:disable RSpec/AnyInstance

      expect(setting.save).to be_truthy
    end

    it do
      expect_any_instance_of(ExternalServices::MinioService).to receive(:try_connection).and_return("an error") # rubocop:disable RSpec/AnyInstance

      expect(setting.save).to be_falsey
    end
  end

  describe "#node_labels" do
    before { setting.node_labels = nil }

    it { expect(setting.node_labels).to be_a(Array) }
  end

  describe "#available_node_labels" do
    context "when docker is running" do
      before { expect(Docker::Node).to receive(:all).and_call_original } # rubocop:disable RSpec/ExpectInHook

      it { expect(setting.available_node_labels).to be_a(Array) }
      it { expect(setting.available_node_labels).to eq(["production"]) }
    end

    context "when docker is not running" do
      before { expect(Docker::Node).to receive(:all).and_raise(Excon::Error) } # rubocop:disable RSpec/ExpectInHook

      it { expect(setting.available_node_labels).to be_a(Array) }
      it { expect(setting.available_node_labels).to be_blank }
    end
  end

  describe "#node_labels=" do
    it do
      setting.node_labels = ["Array", "with", "", "and", nil]
      expect(setting.node_labels).to eq(%w[Array with and])
    end
  end

  describe "#traefik_cert_resolvers=" do
    it do
      setting.traefik_cert_resolvers = "Resolver1,Resolver2\nResolver3"
      expect(setting.traefik_cert_resolvers).to eq(%w[Resolver1 Resolver2 Resolver3])
    end
  end

  describe "#traefik_cert_resolvers" do
    it do
      setting.traefik_cert_resolvers = nil
      expect(setting.traefik_cert_resolvers).to eq([])
    end
  end

  describe "#validate_node_labels" do
    it { is_expected.to allow_value(["production"]).for(:node_labels) }
    it { is_expected.not_to allow_value(["something"]).for(:node_labels) }
  end

  describe "#validate_default_node_lables" do
    before { setting.node_labels = ["allowed"] }

    it { is_expected.to allow_value("allowed").for(:default_production_node_label) }
    it { is_expected.to allow_value("allowed").for(:default_review_node_label) }
    it { is_expected.not_to allow_value("foobar").for(:default_production_node_label) }
    it { is_expected.not_to allow_value("foobar").for(:default_review_node_label) }
  end

  describe "#table_options=" do
    it do
      setting.table_options = ["Array", "with", "", "and", nil]
      expect(setting.table_options).to eq(%w[Array with and])
    end
  end

  describe "#table_class" do
    it do
      setting.table_options = %w[v1 v2]
      expect(setting.table_class).to eq("table-v1 table-v2")
    end
  end
end
