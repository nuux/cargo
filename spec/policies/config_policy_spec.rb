# frozen_string_literal: true

require "rails_helper"

RSpec.describe ConfigPolicy, type: :policy do
  subject { described_class }

  let(:user) { create(:user) }
  let(:admin) { create(:admin) }
  let(:config) { create(:config) }
  let(:related_config) { create(:config, stack: stack) }
  let(:stack) { create(:stack, users: [user]) }

  permissions :index? do
    it { is_expected.to permit(user, Config) }
  end

  permissions :show? do
    it { is_expected.not_to permit(user, config) }
    it { is_expected.to permit(user, related_config) }

    it { is_expected.to permit(admin, config) }
    it { is_expected.to permit(admin, related_config) }
    it { is_expected.not_to permit(admin, Config.new) }
  end

  permissions :new?, :create? do
    it { is_expected.to permit(user, Config) }
  end

  permissions :edit?, :update? do
    it { is_expected.not_to permit(user, config) }
    it { is_expected.to permit(user, related_config) }

    it { is_expected.to permit(admin, config) }
    it { is_expected.to permit(admin, related_config) }
  end

  permissions :destroy? do
    it { is_expected.not_to permit(user, config) }
    it { is_expected.to permit(user, related_config) }

    it { is_expected.to permit(admin, config) }
    it { is_expected.to permit(admin, related_config) }
  end
end
