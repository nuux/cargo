# frozen_string_literal: true

require "rails_helper"

RSpec.describe ServicePolicy, type: :policy do
  subject { described_class }

  let(:user) { create(:user) }
  let(:admin) { create(:admin) }
  let(:service) { create(:service) }
  let(:related_service) { create(:service, stack: stack) }
  let(:stack) { create(:stack, users: [user]) }

  permissions :index? do
    it { is_expected.to permit(user, Service) }
  end

  permissions :show? do
    it { is_expected.not_to permit(user, service) }
    it { is_expected.to permit(user, related_service) }
    it { is_expected.not_to permit(user, Service.new) }

    it { is_expected.to permit(admin, service) }
    it { is_expected.to permit(admin, related_service) }
    it { is_expected.not_to permit(user, Service.new) }
  end

  permissions :new?, :create? do
    it { is_expected.to permit(user, Service) }
  end

  permissions :edit?, :update? do
    it { is_expected.not_to permit(user, service) }
    it { is_expected.to permit(user, related_service) }

    it { is_expected.to permit(admin, service) }
    it { is_expected.to permit(admin, related_service) }
  end

  permissions :duplicate? do
    it { is_expected.not_to permit(user, service) }
    it { is_expected.to permit(user, related_service) }

    it { is_expected.to permit(admin, service) }
    it { is_expected.to permit(admin, related_service) }
  end

  permissions :destroy? do
    it { is_expected.not_to permit(user, service) }
    it { is_expected.to permit(user, related_service) }

    it { is_expected.to permit(admin, service) }
    it { is_expected.to permit(admin, related_service) }
  end
end
