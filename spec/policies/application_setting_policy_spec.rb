# frozen_string_literal: true

require "rails_helper"

RSpec.describe ApplicationSettingPolicy, type: :policy do
  subject { described_class }

  let(:user) { create(:user) }
  let(:admin) { create(:admin) }
  let(:application_setting) { create(:application_setting) }

  permissions :show? do
    it { is_expected.not_to permit(user, application_setting) }
    it { is_expected.to permit(admin, application_setting) }
  end

  permissions :edit?, :update? do
    it { is_expected.not_to permit(user, application_setting) }
    it { is_expected.to permit(admin, application_setting) }
  end
end
