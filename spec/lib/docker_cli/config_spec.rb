# frozen_string_literal: true

require "rails_helper"

RSpec.describe DockerCli::Config do
  let(:name) { "Name" }
  let(:value) { "Value" }

  describe ".create" do
    it { expect(described_class.create(name: name, value: value)).to eq("stdout\nstderr") }

    it do
      described_class.create(name: name, value: value)
      expect(Open3).to have_received(:capture3).with("docker", "config", "create", name.downcase, "-", stdin_data: value)
    end
  end

  describe ".remove" do
    it { expect(described_class.remove(name: name)).to eq("stdout\nstderr") }

    it do
      expect(Open3).to receive(:capture3).with("docker", "config", "ls", "--quiet", "--filter", "name=#{name.downcase}").and_return(["CONFIG_ID_1\nCONFIG_ID_2", ""])
      expect(Open3).to receive(:capture3).with("docker", "config", "rm", "CONFIG_ID_1", "CONFIG_ID_2")

      described_class.remove(name: name)
    end
  end
end
