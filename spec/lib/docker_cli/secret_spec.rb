# frozen_string_literal: true

require "rails_helper"

RSpec.describe DockerCli::Secret do
  let(:name) { "Name" }
  let(:value) { "Value" }

  describe ".create" do
    it { expect(described_class.create(name: name, value: value)).to eq("stdout\nstderr") }

    it do
      described_class.create(name: name, value: value)
      expect(Open3).to have_received(:capture3).with("docker", "secret", "create", name.downcase, "-", stdin_data: value)
    end
  end

  describe ".remove" do
    it { expect(described_class.remove(name: name)).to eq("stdout\nstderr") }

    it do
      described_class.remove(name: name)
      expect(Open3).to have_received(:capture3).with("docker", "secret", "rm", name.downcase)
    end
  end
end
