# frozen_string_literal: true

require "rails_helper"

RSpec.describe DockerApi::Container do
  subject(:object) { described_class.new(stack: stack, container: container) }

  let(:stack) { instance_double("DockerApi::Stack", name: "test", containers: [], tasks: []) }
  let(:container) { instance_double("Docker::Container", id: "id", info: info) }
  let(:info) do
    {
      Id: "id",
      Image: "image:latest@sha",
      State: "state",
      Status: "status",
      Ports: [{ PrivatePort: "80" }],
      Created: "2019-01-01".to_datetime.to_i,
      Labels: {
        "com.docker.swarm.service.id" => "service_id",
        "com.docker.swarm.service.name" => "test_name",
        "com.docker.swarm.task.id" => "task_id"
      }
    }.deep_stringify_keys
  end

  describe "#id" do
    it { expect(object.id).to eq("id") }
  end

  describe "#name" do
    it { expect(object.name).to eq("name") }
  end

  describe "#image" do
    it { expect(object.image).to eq("image:latest") }
  end

  describe "#state" do
    it { expect(object.state).to eq("state") }
  end

  describe "#status" do
    it { expect(object.status).to eq("status") }
  end

  describe "#healthy?" do
    it { expect(object).to be_healthy }
  end

  describe "#ports" do
    it { expect(object.ports).to eq([80]) }
  end

  describe "#created_at" do
    it { expect(object.created_at).to eq("2019-01-01".to_datetime) }
  end

  describe "#service_id" do
    it { expect(object.service_id).to eq("service_id") }
  end

  describe "#task_id" do
    it { expect(object.task_id).to eq("task_id") }
  end
end
