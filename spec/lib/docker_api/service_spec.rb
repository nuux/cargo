# frozen_string_literal: true

require "rails_helper"

RSpec.describe DockerApi::Service do
  subject(:object) { described_class.new(stack: stack, service: service) }

  let(:stack) { instance_double("DockerApi::Stack", name: "test", containers: [], tasks: []) }
  let(:service) { instance_double("Docker::Service", id: "id", info: info, streaming_logs: true) }
  let(:info) do
    {
      ID: "id",
      Spec: {
        Name: "test_name",
        TaskTemplate: {
          ContainerSpec: {
            Image: "image:latest@sha",
            Healthcheck: { foobar: true }
          }
        },
        Mode: {
          Replicated: {
            Replicas: "2"
          }
        },
        Labels: {
          "traefik.test1.port" => "801",
          "traefik.test1.frontend.rule" => "host:localhost,cargo",
          "traefik.http.services.service2.loadbalancer.server.port" => "802",
          "traefik.http.routers.route2.service" => "service2",
          "traefik.http.routers.route2.rule" => "Host(`d1`,`d2`)"
        }
      },
      CreatedAt: "2019-01-01",
      UpdatedAt: "2019-01-01"
    }.deep_stringify_keys
  end

  describe "#containers" do
    it do
      expect(stack).to receive(:containers).and_return([instance_double("DockerApi::Container", service_id: "id"), instance_double("DockerApi::Container", service_id: "other-id")])

      containers = object.containers

      expect(containers.count).to eq(1)
    end
  end

  describe "#tasks" do
    it do
      expect(stack).to receive(:tasks).and_return([instance_double("DockerApi::Task", service_id: "id"), instance_double("DockerApi::Task", service_id: "other-id")])

      tasks = object.tasks

      expect(tasks.count).to eq(1)
    end
  end

  describe "#streaming_logs" do
    it do
      expect(object.streaming_logs(wait: 15) { nil }).to be_truthy

      expect(service).to have_received(:streaming_logs).with(stdout: true, stderr: true, follow: true, wait: 15)
    end
  end

  describe "#id" do
    it { expect(object.id).to eq("id") }
  end

  describe "#name" do
    it { expect(object.name).to eq("name") }
  end

  describe "#image" do
    it { expect(object.image).to eq("image:latest") }
  end

  describe "#replicas" do
    it { expect(object.replicas).to eq(2) }
  end

  describe "#ports" do
    context "when traefik_version is v1" do
      before { allow(Current.settings).to receive(:traefik_version).and_return("v1") }

      it { expect(object.ports).to eq([{ number: 801, hosts: %w[localhost cargo] }]) }
    end

    context "when traefik_version is v2" do
      before { allow(Current.settings).to receive(:traefik_version).and_return("v2") }

      it { expect(object.ports).to eq([{ number: 802, hosts: %w[d1 d2] }]) }
    end
  end

  describe "#container_ports" do
    it do
      expect(stack).to receive(:containers).and_return([instance_double("DockerApi::Container", service_id: "id", ports: [80])])

      expect(object.container_ports).to eq([80])
    end
  end

  describe "#healthcheck?" do
    it { expect(object).to be_healthcheck }
  end

  # rubocop:disable RSpec/SubjectStub
  describe "#status" do
    context "when healthcheck" do
      before { allow(object).to receive(:healthcheck?).and_return(true) }

      it do
        allow(object).to receive(:containers).and_return([instance_double("DockerApi::Container", healthy?: true)])

        expect(object.status).to eq(:success)
      end

      it do
        allow(object).to receive(:containers).and_return([instance_double("DockerApi::Container", healthy?: true), instance_double("DockerApi::Container", healthy?: false)])

        expect(object.status).to eq(:warning)
      end

      it do
        allow(object).to receive(:containers).and_return([instance_double("DockerApi::Container", healthy?: false)])

        expect(object.status).to eq(:danger)
      end
    end

    context "without healthcheck" do
      before { allow(object).to receive(:healthcheck?).and_return(false) }

      it do
        allow(object).to receive(:tasks).and_return([instance_double("DockerApi::Task", ok?: true), instance_double("DockerApi::Task", ok?: true)])

        expect(object.status).to eq(:success)
      end

      it do
        allow(object).to receive(:tasks).and_return([instance_double("DockerApi::Task", ok?: false), instance_double("DockerApi::Task", ok?: true)])

        expect(object.status).to eq(:danger)
      end
    end
  end
  # rubocop:enable RSpec/SubjectStub

  describe "#created_at" do
    it { expect(object.created_at).to eq("2019-01-01".to_datetime) }
  end

  describe "#updated_at" do
    it { expect(object.updated_at).to eq("2019-01-01".to_datetime) }
  end
end
