# frozen_string_literal: true

require "rails_helper"

RSpec.describe DockerApi::Task do
  subject(:object) { described_class.new(stack: stack, task: task) }

  let(:stack) { instance_double("DockerApi::Stack", name: "test", containers: [], tasks: []) }
  let(:task) { instance_double("Docker::Task", id: "id", info: info) }
  let(:info) do
    {
      ID: "id",
      Status: {
        State: "state"
      },
      DesiredState: "desired_state",
      Spec: {
        ContainerSpec: {
          Image: "image:latest@sha"
        }
      },
      CreatedAt: "2019-01-01",
      ServiceID: "service_id"
    }.deep_stringify_keys
  end

  describe "#id" do
    it { expect(object.id).to eq("id") }
  end

  describe "#containers" do
    it do
      expect(stack).to receive(:containers).and_return([instance_double("DockerApi::Container", task_id: "id"), instance_double("DockerApi::Container", task_id: "other-id")])

      containers = object.containers

      expect(containers.count).to eq(1)
    end
  end

  describe "#state" do
    it { expect(object.state).to eq("state") }
  end

  describe "#desired_state" do
    it { expect(object.desired_state).to eq("desired_state") }
  end

  describe "#image" do
    it { expect(object.image).to eq("image:latest") }
  end

  describe "#created_at" do
    it { expect(object.created_at).to eq("2019-01-01".to_datetime) }
  end

  describe "#ok?" do
    it { expect(object).not_to be_ok }
  end

  describe "#running?" do
    it { expect(object).not_to be_running }
  end

  describe "#service_id" do
    it { expect(object.service_id).to eq("service_id") }
  end
end
