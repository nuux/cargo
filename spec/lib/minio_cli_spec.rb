# frozen_string_literal: true

require "rails_helper"

RSpec.describe MinioCli do
  describe ".make_bucket" do
    it do
      expect(described_class).to receive(:mc).with("mb", "cargo/name")

      described_class.make_bucket("name")
    end
  end

  describe ".remove_bucket" do
    it do
      expect(described_class).to receive(:mc).with("rb", "cargo/name")

      described_class.remove_bucket("name")
    end
  end

  describe ".add_user" do
    it do
      expect(described_class).to receive(:mc).with("admin", "user", "add", "cargo", "access_key", "secret_key")

      described_class.add_user("access_key", "secret_key")
    end
  end

  describe ".remove_user" do
    it do
      expect(described_class).to receive(:mc).with("admin", "user", "remove", "cargo", "access_key")

      described_class.remove_user("access_key")
    end
  end

  describe ".add_policy" do
    it do
      expect(described_class).to receive(:mc).with("admin", "policy", "add", "cargo", "name", "file")

      described_class.add_policy("name", "file")
    end
  end

  describe ".remove_policy" do
    it do
      expect(described_class).to receive(:mc).with("admin", "policy", "remove", "cargo", "name")

      described_class.remove_policy("name")
    end
  end

  describe ".set_policy" do
    it do
      expect(described_class).to receive(:mc).with("admin", "policy", "set", "cargo", "name", "user=user")

      described_class.set_policy("name", "user")
    end
  end

  describe ".add_host" do
    it do
      expect(described_class).to receive(:mc).with("config", "host", "add", "cargo", "host", "access_key", "secret_key")

      described_class.add_host("host", "access_key", "secret_key")
    end
  end

  describe ".remove_host" do
    it do
      expect(described_class).to receive(:mc).with("config", "host", "remove", "cargo")

      described_class.remove_host
    end
  end

  describe ".info" do
    it do
      expect(described_class).to receive(:mc).with("admin", "info", "cargo")

      described_class.info
    end
  end

  describe ".mc", skip_open3: true do
    it do
      expect(Open3).to receive(:capture2e).with("mc", "--json", "arg1", "arg2").and_return([{ status: "success", error: "" }.to_json, instance_double(Process::Status, success?: true)])

      expect(described_class.send(:mc, "arg1", "arg2")).to be_a(ActiveSupport::InheritableOptions)
    end

    it do
      expect(Open3).to receive(:capture2e).and_return([{ status: "success", error: { message: "error message" } }.to_json, instance_double(Process::Status, success?: false)])

      expect { described_class.send(:mc, "arg1", "arg2") }.to raise_error(MinioCli::Error, "error message")
    end

    it do
      expect(Open3).to receive(:capture2e).and_return([{ status: "success", error: { message: "error message", cause: { message: "cause" } } }.to_json, instance_double(Process::Status, success?: false)])

      expect { described_class.send(:mc, "arg1", "arg2") }.to raise_error(MinioCli::Error, "error message cause")
    end

    it do
      expect(Open3).to receive(:capture2e).and_return([{ status: "success", error: { other: "error message" } }.to_json, instance_double(Process::Status, success?: false)])

      expect { described_class.send(:mc, "arg1", "arg2") }.to raise_error(MinioCli::Error, '{:other=>"error message"}')
    end

    it do
      expect(Open3).to receive(:capture2e).and_return(["invalid JSON", instance_double(Process::Status, success?: false)])

      expect { described_class.send(:mc, "arg1", "arg2") }.to raise_error(MinioCli::Error, "JSON::ParserError")
    end
  end
end
