# frozen_string_literal: true

require "rails_helper"

RSpec.describe DockerCli do
  let(:options) { { url: "https://registry.example.org", username: "username", password: "password" } }

  describe ".login" do
    it { expect(described_class.login(options)).to eq("stdout\nstderr") }

    it do
      described_class.login(options)
      expect(Open3).to have_received(:capture3).with("docker", "login", "--username", options[:username], "--password-stdin", options[:url], stdin_data: options[:password])
    end
  end
end
