# frozen_string_literal: true

require "rails_helper"

RSpec.describe Prometheus do
  describe ".client" do
    it { expect(described_class.client).to be_instance_of(Prometheus::Client) }
  end
end
