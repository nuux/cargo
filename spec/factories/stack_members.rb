# frozen_string_literal: true

FactoryBot.define do
  factory :stack_member do
    association :stack, factory: :stack
    association :user, factory: :user
  end
end
