# frozen_string_literal: true

FactoryBot.define do
  factory :config_claim do
    association :config, factory: :config
    association :service, factory: :service
    sequence(:target) { |n| "target#{n}" }
  end
end
