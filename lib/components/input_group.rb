# frozen_string_literal: true

# Source: https://github.com/rafaelfranca/simple_form-bootstrap/blob/master/lib/components/input_group_component.rb

# custom component requires input group wrapper
module Components
  module InputGroup
    # def prepend(_wrapper_options = nil)
    #   span_tag = content_tag(:span, options[:prepend], class: "input-group-text")
    #   template.content_tag(:div, span_tag, class: "input-group-prepend")
    # end

    def append(_wrapper_options = nil)
      span_tag = tag.span(options[:append], class: "input-group-text")
      template.tag.div(span_tag, class: "input-group-append")
    end
  end
end
# Register the component in Simple Form.
SimpleForm.include_component(Components::InputGroup)
