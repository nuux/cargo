# frozen_string_literal: true

namespace :docker do
  task db: :environment do
    connection_tries ||= 10
    database_tries ||= 3

    ActiveRecord::Base.establish_connection

    Rake::Task["db:migrate"].invoke if ActiveRecord::Base.connection.migration_context.needs_migration?

    exit 0
  rescue ActiveRecord::NoDatabaseError => exception
    Rails.logger.warn "#{exception.class}: #{exception.message}"
    exit 2 if (database_tries -= 1).zero?

    Rake::Task["db:create"].invoke

    retry
  rescue StandardError => exception
    Rails.logger.warn "#{exception.class}: #{exception.message}"
    exit 1 if (connection_tries -= 1).zero?

    Rails.logger.info "Retrying DB connection #{connection_tries} more times..."
    sleep ENV.fetch("DB_SETUP_WAIT", "2").to_i

    retry
  end
end
