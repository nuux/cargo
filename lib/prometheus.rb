# frozen_string_literal: true

module Prometheus
  extend ActiveSupport::Autoload

  autoload :Client

  class << self
    def client
      Prometheus::Client.new
    end
  end
end
