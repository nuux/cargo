# frozen_string_literal: true

module DockerCli
  class Config
    class << self
      def create(name:, value:)
        stdout, stderr = Open3.capture3("docker", "config", "create", name.downcase, "-", stdin_data: value)

        [stdout.presence, stderr.presence].compact.join("\n").chomp
      end

      def remove(name:)
        stdout, stderr = Open3.capture3("docker", "config", "ls", "--quiet", "--filter", "name=#{name}".downcase)
        stdout, stderr = Open3.capture3("docker", "config", "rm", *stdout.split) if stdout.present? && stderr.blank?

        [stdout.presence, stderr.presence].compact.join("\n").chomp
      end
    end
  end
end
