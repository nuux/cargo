#!/usr/bin/env ruby
# frozen_string_literal: true

require "httparty"
require "thor"
require "uri"

class CargoCli < Thor
  def self.exit_on_failure?
    true
  end

  class_option :host, type: :string, default: ENV["CARGO_HOST"], banner: "$CARGO_HOST", desc: "Cargo host name (default: $CARGO_HOST)"
  class_option :stack, type: :string, default: ENV["CARGO_STACK_ID"], banner: "$CARGO_STACK_ID", desc: "Cargo stack id (default: $CARGO_STACK_ID)"
  class_option :token, type: :string, default: ENV["CARGO_STACK_TOKEN"], banner: "$CARGO_STACK_TOKEN", desc: "Cargo stack token (default: $CARGO_STACK_TOKEN)"

  desc "deploy ENVIRONMENT", "Deploy environment"
  option :version, type: :string, desc: "Deployment version"
  option :tags, type: :hash, desc: "Deployment service tags (e.g.: \"web:master sidekiq:latest\")"
  option :images, type: :hash, desc: %(Deployment service image (e.g.: "web:registry.gitlab.com/nuux/cargo:2.5.0 sidekiq:registry.gitlab.com/nuux/cargo:2.5.0"))
  def deploy(environment) # rubocop:disable Metrics/MethodLength
    validates_class_options

    response(
      HTTParty
        .post(
          "#{base_uri}/deploy",
          headers: headers,
          format: :json,
          body: {
            environment: environment,
            version: options[:version],
            tags: sanitizes(options[:tags]),
            images: sanitizes(options[:images])
          }.compact
        )
    )
  end

  desc "remove ENVIRONMENT", "Remove environment"
  def remove(environment) # rubocop:disable Metrics/MethodLength
    validates_class_options

    response(
      HTTParty
        .delete(
          "#{base_uri}/remove",
          headers: headers,
          format: :json,
          body: {
            environment: environment
          }
        )
    )
  end

private

  def response(response)
    body = response.parsed_response
    puts "Status: #{body['status']}"
    puts "Message: #{body['errors'].join(', ')}" if body["errors"]

    exit 2 unless [200, 201].include?(response.code)
  rescue StandardError => exception
    puts "#{exception.class}: #{exception.message}"

    exit 3
  end

  def tags
    return unless options.fetch(:tags, false)

    options.fetch(:tags, {}).map do |key, value|
      key = key.gsub(/[[:space:]]+/, " ").strip
      value = value.gsub(/[[:space:]]+/, " ").strip

      next if value.to_s.empty?

      [key, value]
    end.to_h
  end

  def sanitizes(hash)
    return if hash.nil? || hash.empty? # rubocop:disable Rails/Blank

    hash.map do |key, value|
      key = key.gsub(/[[:space:]]+/, " ").strip
      value = value.gsub(/[[:space:]]+/, " ").strip

      next if value.to_s.empty?

      [key, value]
    end.to_h
  end

  def headers
    { "X-TOKEN" => options.fetch(:token) }
  end

  def base_uri
    "#{options.fetch(:host).chomp('/')}/api/v1/stacks/#{options.fetch(:stack)}"
  end

  def validates_class_options # rubocop:disable Metrics/AbcSize
    raise RequiredArgumentMissingError, "No value provided for required options '--host'" if options[:host].to_s.empty?
    raise RequiredArgumentMissingError, "No value provided for required options '--stack'" if options[:stack].to_s.empty?
    raise RequiredArgumentMissingError, "No value provided for required options '--token'" if options[:token].to_s.empty?
    raise MalformattedArgumentError, "Expected a valid uri for '--host" unless options.host.match? URI::DEFAULT_PARSER.regexp[:ABS_URI]
  end
end

CargoCli.start(ARGV)
