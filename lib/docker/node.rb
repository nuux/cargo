# frozen_string_literal: true

module Docker
  class Node
    include Docker::Base

    def self.all(opts = {}, conn = Docker.connection)
      hashes = Docker::Util.parse_json(conn.get("/nodes", opts)) || []
      hashes.map { |hash| new(conn, hash) }
    end

    private_class_method :new
  end
end
