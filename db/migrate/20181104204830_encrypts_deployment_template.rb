# frozen_string_literal: true

class EncryptsDeploymentTemplate < ActiveRecord::Migration[5.2]
  def up
    change_table :deployments, bulk: true do |t|
      t.text :encrypted_template
      t.string :encrypted_template_iv
      t.remove :template
    end
  end
end
