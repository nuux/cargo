# frozen_string_literal: true

class AddNodeLabelsToStacks < ActiveRecord::Migration[6.0]
  def change
    change_table :stacks, bulk: true do |t|
      t.string :default_production_node_label
      t.string :default_review_node_label
    end
  end
end
