# frozen_string_literal: true

class AddMinioProductionBucketNameToExternalServices < ActiveRecord::Migration[6.0]
  def change
    change_table :external_services, bulk: true do |t|
      t.string :minio_production_bucket_name, index: { unique: true }
    end
  end
end
