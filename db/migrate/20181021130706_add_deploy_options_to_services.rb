# frozen_string_literal: true

class AddDeployOptionsToServices < ActiveRecord::Migration[5.2]
  def change
    change_table :services, bulk: true do |t|
      t.integer :replicas, null: false, default: 1

      t.integer :restart_condition, null: false, default: 1
      t.integer :restart_delay, null: false, default: 5
      t.integer :restart_window, null: false, default: 5

      t.integer :rollback_parallelism, null: false, default: 1
      t.integer :rollback_delay, null: false, default: 5
      t.integer :rollback_failure_action, null: false, default: 0
      t.integer :rollback_monitor, null: false, default: 5
      t.integer :rollback_order, null: false, default: 1

      t.integer :update_parallelism, null: false, default: 1
      t.integer :update_delay, null: false, default: 5
      t.integer :update_failure_action, null: false, default: 2
      t.integer :update_monitor, null: false, default: 5
      t.integer :update_order, null: false, default: 1
    end
  end
end
