# frozen_string_literal: true

class CreateExternalServices < ActiveRecord::Migration[6.0]
  def change
    create_table :external_services, id: :uuid do |t|
      t.references :stack, foreign_key: true, null: false, type: :uuid
      t.integer :service_type, default: 0, null: false
      t.string :name, null: false
      t.boolean :clean_review, default: true, null: false

      t.index %i[stack_id name], unique: true

      t.timestamps
    end
  end
end
