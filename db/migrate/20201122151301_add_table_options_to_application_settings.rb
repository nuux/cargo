# frozen_string_literal: true

class AddTableOptionsToApplicationSettings < ActiveRecord::Migration[6.0]
  def change
    add_column :application_settings, :table_options, :string, array: true, default: "{striped}"
  end
end
