# frozen_string_literal: true

class AddPrimaryColorToApplicationSettings < ActiveRecord::Migration[6.0]
  def change
    add_column :application_settings, :primary_color, :string
  end
end
