# frozen_string_literal: true

class CreateFrontends < ActiveRecord::Migration[5.2]
  def change
    create_table :frontends, id: :uuid do |t|
      t.references :stack, foreign_key: true, null: false, type: :uuid
      t.references :service, foreign_key: true, null: false, type: :uuid
      t.string :name, null: false
      t.integer :port, null: false
      t.text :hosts, null: false

      t.index %i[service_id name], unique: true

      t.timestamps
    end
  end
end
