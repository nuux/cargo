# frozen_string_literal: true

class AddFrontendHostSchemaToApplicationSettings < ActiveRecord::Migration[6.0]
  def change
    add_column :application_settings, :frontend_host_schema, :integer, null: false, default: 0
  end
end
