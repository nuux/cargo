# frozen_string_literal: true

class AddNodeLabelsToApplicationSettings < ActiveRecord::Migration[6.0]
  def change
    change_table :application_settings, bulk: true do |t|
      t.string :node_labels, array: true
      t.string :default_production_node_label
      t.string :default_review_node_label
    end
  end
end
