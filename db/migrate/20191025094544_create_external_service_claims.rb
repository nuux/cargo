# frozen_string_literal: true

class CreateExternalServiceClaims < ActiveRecord::Migration[6.0]
  def change
    create_table :external_service_claims, id: :uuid do |t|
      t.references :external_service, null: false, foreign_key: true, type: :uuid
      t.references :service, null: false, foreign_key: true, type: :uuid
      t.string :target, null: false

      t.index %i[service_id target], unique: true
      t.index %i[external_service_id service_id], unique: true, name: "index_external_service_claims_on_es_id_and_s_id"

      t.timestamps
    end
  end
end
