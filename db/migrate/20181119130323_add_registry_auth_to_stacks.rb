# frozen_string_literal: true

class AddRegistryAuthToStacks < ActiveRecord::Migration[5.2]
  def change
    change_table :stacks, bulk: true do |t|
      t.string :registry_url
      t.string :registry_username
      t.string :encrypted_registry_password
      t.string :encrypted_registry_password_iv
    end
  end
end
