# frozen_string_literal: true

class ChangePrimaryColorColumnNull < ActiveRecord::Migration[6.0]
  def change
    ApplicationSetting.update_all(primary_color: "#009cff")

    change_column_null :application_settings, :primary_color, false
  end
end
