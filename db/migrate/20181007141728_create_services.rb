# frozen_string_literal: true

class CreateServices < ActiveRecord::Migration[5.2]
  def change
    create_table :services, id: :uuid do |t|
      t.references :stack, foreign_key: true, null: false, type: :uuid
      t.string :name, null: false
      t.string :image, null: false
      t.string :command

      t.index %i[stack_id name], unique: true

      t.timestamps
    end
  end
end
