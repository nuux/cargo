# frozen_string_literal: true

class RemoveCleanReviewFromExternalServices < ActiveRecord::Migration[6.0]
  def change
    remove_column :external_services, :clean_review, :boolean, default: true, null: false
  end
end
