# frozen_string_literal: true

module Template
  class ImportService
    def initialize(file:, stack:, inputs:)
      @file = file
      @stack = stack
      @inputs = inputs
    end

    def import
      return false unless @stack.fresh?

      ActiveRecord::Base.transaction do
        @environments = create_environments
        @services = create_services
        @frontends = create_frontends
      end

      true
    rescue ActiveRecord::ActiveRecordError => exception
      Rails.logger.info exception.full_message
      false
    end

  private

    def create_environments
      template.fetch(:environments, {}).map do |name, value|
        if value.is_a?(Hash)
          review_value = value[:review]
          production_value = value[:production]
        else
          production_value = review_value = value
        end
        environment = @stack.configs.create! config_type: :environment, name: name, value: production_value, review_value: review_value

        [name, environment]
      end.to_h
    end

    def create_services # rubocop:disable Metrics/AbcSize
      template.fetch(:services, {}).map do |name, options|
        service = @stack.services.create! options.slice(*Service.attribute_names).merge("name" => name)

        options.fetch("environments", {}).each do |environment, target|
          service.config_claims.create!(config: @environments.fetch(environment), target: target)
        end

        options.fetch("volumes", {}).each do |volume_name, target|
          service.volume_claims.create!(volume: volume(volume_name), target: target)
        end

        [name, service]
      end.to_h
    end

    def create_frontends
      template.fetch(:frontends, {}).map do |name, options|
        frontend = @stack.frontends.create! options.slice(*Frontend.attribute_names).merge("name" => name, "service" => @services.fetch(options.fetch("service")))

        [name, frontend]
      end.to_h
    end

    def volume(name)
      @volumes ||= {}
      @volumes[name] ||= @stack.volumes.create! name: name
    end

    def template
      @template ||= Template::RenderService.new(file: @file, stack: @stack, inputs: @inputs).render
    end
  end
end
