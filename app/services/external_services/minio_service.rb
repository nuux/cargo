# frozen_string_literal: true

module ExternalServices
  class MinioService
    class << self
      delegate :create_bucket, :remove_bucket, to: :new
    end

    def initialize(setting = Current.settings)
      @minio_host = setting.minio_host
      @minio_access_key = setting.minio_access_key
      @minio_secret_key = setting.minio_secret_key
    end

    def try_connection
      minio_context do
        MinioCli.info.status
      end
    rescue MinioCli::Error => exception
      exception.message
    end

    def create_bucket(bucket_name)
      access_key = SecureRandom.base58(32)
      secret_key = SecureRandom.base58(64)
      policy_name = "#{bucket_name}-policy"

      make_bucket(bucket_name: bucket_name, access_key: access_key, secret_key: secret_key, policy_name: policy_name)

      { bucket_name: bucket_name, access_key: access_key, secret_key: secret_key, policy_name: policy_name }
    end

    def remove_bucket(access_key:, bucket_name:, policy_name:)
      minio_context do
        MinioCli.remove_user(access_key)
        MinioCli.remove_policy(policy_name)
        MinioCli.remove_bucket(bucket_name) if bucket_name
      end
    end

  private

    def minio_context
      MinioCli.add_host(@minio_host, @minio_access_key, @minio_secret_key)
      yield
    ensure
      MinioCli.remove_host
    end

    def make_bucket(bucket_name:, access_key:, secret_key:, policy_name:)
      minio_context do
        MinioCli.make_bucket(bucket_name)
        MinioCli.add_user(access_key, secret_key)
        policy_document(policy_name, bucket_name) { |file| MinioCli.add_policy(policy_name, file.path) }
        MinioCli.set_policy(policy_name, access_key)
      end
    end

    def policy_document(policy_name, bucket_name)
      Tempfile.create([policy_name, ".json"]) do |file|
        file.write policy_document_json(policy_name, bucket_name)
        file.rewind

        yield(file)
      end
    end

    def policy_document_json(policy_name, bucket_name)
      {
        Version: "2012-10-17",
        Statement: [{
          Action: ["s3:ListBucket", "s3:GetObject", "s3:PutObject", "s3:DeleteObject"],
          Effect: "Allow",
          Resource: ["arn:aws:s3:::#{bucket_name}/*"],
          Sid: policy_name
        }]
      }.to_json
    end
  end
end
