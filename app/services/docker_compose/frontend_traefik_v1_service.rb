# frozen_string_literal: true

module DockerCompose
  class FrontendTraefikV1Service # rubocop:disable Metrics/ClassLength
    HOSTNAME_REGEXP = /\A((xn--)?[a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,}\Z/i.freeze

    def initialize(frontend:, deployment:)
      @frontend = frontend
      @deployment = deployment
      @stack = deployment.stack
      @service = frontend.service
    end

    def labels # rubocop:disable Metrics/MethodLength
      {
        "traefik.enable": "true",
        "traefik.#{name}.port": @frontend.port,
        "traefik.#{name}.frontend.rule": "Host:#{traefik_hosts}",
        "traefik.#{name}.frontend.passHostHeader": "true"
      }
        .merge(auth_basic)
        .merge(errors)
        .merge(redirect)
        .merge(custom_headers)
        .merge(hsts)
    end

    def environment
      {
        "CARGO_FRONTEND_#{name.upcase}_HOSTS" => traefik_hosts
      }
    end

  private

    def name
      @frontend.name.downcase
    end

    def traefik_hosts
      hosts = generate_hosts

      hosts.split(",").each do |host|
        raise DockerComposeService::Error, "The hostname \"#{host}\" is too long (max. 64 characters)" if host.size > 64
        raise DockerComposeService::Error, "The hostname \"#{host}\" is invalid" unless host.match?(HOSTNAME_REGEXP)
      end

      hosts
    end

    def generate_hosts
      if @deployment.production? && @frontend.hosts?
        @frontend.hosts.to_s.split.join(",")
      else
        [subdomain, Current.settings.host].flatten.join(".")
      end
    end

    def auth_basic
      return {} unless @frontend.auth_basic?

      {
        "traefik.#{name}.frontend.auth.basic.users": auth_basic_users,
        "traefik.#{name}.frontend.auth.basic.removeHeader": @frontend.auth_basic_remove_header.to_s
      }
    end

    def errors
      return {} unless Current.settings.backend_name

      {
        "traefik.#{name}.frontend.errors.cargo.backend": Current.settings.backend_name,
        "traefik.#{name}.frontend.errors.cargo.status": "502,503,504",
        "traefik.#{name}.frontend.errors.cargo.query": "/errors/service_unavailable"
      }
    end

    def redirect
      return {} unless @frontend.redirect?

      {
        "traefik.#{name}.frontend.redirect.regex": @frontend.redirect_regex,
        "traefik.#{name}.frontend.redirect.replacement": @frontend.redirect_replacement,
        "traefik.#{name}.frontend.redirect.permanent": @frontend.redirect_permanent.to_s
      }
    end

    def custom_headers
      {
        "traefik.#{name}.frontend.customRequestHeaders": transform_header(@frontend.request_headers),
        "traefik.#{name}.frontend.customResponseHeaders": transform_header(@frontend.response_headers)
      }.compact
    end

    def hsts
      return {} unless @frontend.hsts?
      return {} unless @deployment.production?

      {
        "traefik.#{name}.frontend.headers.forceSTSHeader": "true",
        "traefik.#{name}.frontend.headers.STSSeconds": @frontend.hsts_max_age,
        "traefik.#{name}.frontend.headers.STSIncludeSubdomains": @frontend.hsts_include_subdomain.to_s
      }
    end

    def subdomain
      if Current.settings.frontend_host_schema == "single_subdomain"
        [@stack.short_name, @frontend.name, @deployment.name].join("-")
      else
        multiple_subdomains
      end
    end

    def multiple_subdomains
      if @stack.frontends.count == 1
        subdomain_stack
      elsif @stack.frontends.where(name: name).count == 1
        subdomain_frontend
      else
        subdomain_full
      end.compact.map(&:downcase).map(&:parameterize)
    end

    def subdomain_stack
      [@deployment.name, @stack.short_name]
    end

    def subdomain_frontend
      [name, @deployment.name, @stack.short_name]
    end

    def subdomain_full
      [name, @service.name, @deployment.name, @stack.short_name]
    end

    def auth_basic_users
      YAML.safe_load(@frontend.auth_basic_users).map do |key, value|
        "#{key}:#{BCrypt::Password.create value.squish}"
      end.join(",")
    rescue StandardError => exception
      Rails.logger.warn "FrontendAuthBasicUserException: #{exception.class}: #{exception.message}"

      nil
    end

    def transform_header(headers)
      return if headers.blank?

      YAML.safe_load(headers).map do |key, value|
        "#{key}:#{value}"
      end.join("||")
    rescue StandardError => exception
      Rails.logger.warn "FrontendTransformHandersException: #{exception.class}: #{exception.message}"

      nil
    end
  end
end
