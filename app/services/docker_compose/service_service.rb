# frozen_string_literal: true

module DockerCompose
  class ServiceService # rubocop:disable Metrics/ClassLength
    attr_reader :volumes, :configs

    def initialize(service:, deployment:) # rubocop:disable Metrics/AbcSize, Metrics/MethodLength
      @service = service
      @deployment = deployment
      @volumes = service.volume_claims.map { |volume_claim| DockerCompose::VolumeService.new(volume_claim: volume_claim) }
      @configs = service.config_claims.map { |config_claim| DockerCompose::ConfigService.new(config_claim: config_claim, deployment: @deployment) }
      @external_services = service.external_service_claims.map { |external_service_claim| DockerCompose::ExternalServiceService.new(external_service_claim: external_service_claim, deployment: @deployment) }

      @frontends = service.frontends.map do |frontend|
        if Current.settings.traefik_version == "v2"
          DockerCompose::FrontendTraefikV2Service.new(frontend: frontend, deployment: @deployment)
        else
          DockerCompose::FrontendTraefikV1Service.new(frontend: frontend, deployment: @deployment)
        end
      end
    end

    def service
      { name => options.compact }
    end

  private

    def options # rubocop:disable Metrics/MethodLength
      {
        image: image,
        command: @service.command.presence,
        volumes: volume_claims,
        networks: networks,
        configs: config_claims,
        environment: environments,
        secrets: secrets,
        deploy: deploy,
        healthcheck: healthcheck
      }.compact
    end

    def image
      if @deployment.images.present?
        use_image_from_images
      else
        build_image_with_tags
      end
    end

    def name
      @service.name.downcase
    end

    def volume_claims
      @volumes.map(&:service_claim).presence
    end

    def networks
      if @service.frontends.any?
        ["overlay", Current.settings.traefik_network]
      else
        ["overlay"]
      end
    end

    def config_claims
      @configs.map(&:config).compact.presence
    end

    def environments
      {
        "CARGO_ENV" => @deployment.name
      }.merge(
        @configs.map(&:environment).compact.inject(:merge).to_h,
        @external_services.map(&:environment).compact.inject(:merge).to_h,
        @frontends.map(&:environment).compact.inject(:merge).to_h
      )
    end

    def secrets
      @configs.map(&:secret).compact.presence
    end

    def deploy
      {
        placement: deploy_placement,
        replicas: @service.replicas,
        rollback_config: deploy_rollback_config,
        update_config: deploy_update_config,
        restart_policy: deploy_restart_policy,
        labels: @frontends.map(&:labels).inject(:merge)
      }.compact
    end

    def deploy_placement
      node_label = @deployment.environment.node_label

      return if node_label.blank?

      { constraints: ["node.labels.cargo_#{node_label} == true"] }
    end

    def deploy_rollback_config
      {
        parallelism: @service.rollback_parallelism,
        delay: "#{@service.rollback_delay}s",
        failure_action: @service.rollback_failure_action,
        monitor: "#{@service.rollback_monitor}s",
        order: @service.rollback_order
      }
    end

    def deploy_update_config
      {
        parallelism: @service.update_parallelism,
        delay: "#{@service.update_delay}s",
        failure_action: @service.update_failure_action,
        monitor: "#{@service.update_monitor}s",
        order: @service.update_order
      }
    end

    def deploy_restart_policy
      {
        condition: @service.restart_condition,
        delay: "#{@service.restart_delay}s",
        window: "#{@service.restart_window}s"
      }
    end

    def healthcheck
      return unless @service.healthcheck?

      {
        test: ["CMD-SHELL", @service.healthcheck_test],
        interval: "#{@service.healthcheck_interval}s",
        timeout: "#{@service.healthcheck_timeout}s",
        retries: @service.healthcheck_retries,
        start_period: "#{@service.healthcheck_start_period}s"
      }
    end

    def use_image_from_images
      image = @deployment.images.try(:[], name)

      image.presence || build_image_with_tags
    end

    def build_image_with_tags
      image =
        if @service.image.include?(":")
          @service.image.split(":")
        else
          [@service.image, "latest"]
        end

      tag = @deployment.tags.try(:[], name)

      image[-1] = tag if tag.present?
      image.join(":")
    end
  end
end
