# frozen_string_literal: true

class LogsStreamService
  TIMEOUT = (Rails.env.test? ? 0 : 30).seconds.freeze

  def initialize(service:, stream:, params: {})
    @service = service
    @stream = stream
    @params = params
    @start_at = nil

    @closed = false
  end

  def run
    return if @closed

    @start_at = Time.current
    @service.streaming_logs(since: since, wait: TIMEOUT) do |_stream, chuck|
      write(chuck)

      raise Excon::Errors::Timeout if abort?
    end
  ensure
    close
  end

  def close
    return if @closed

    @closed = true
    @stream.close
  end

private

  def write(message)
    @stream.write(message)
  end

  def abort?
    return true if @closed

    @start_at < TIMEOUT.ago
  end

  def since
    return if @params[:since].blank?

    Time.zone.parse(@params[:since]).to_i
  rescue ArgumentError
    nil
  end
end
