import { Controller } from "stimulus"

export default class extends Controller {
  static targets = [ "wrapper", "messages", "button" ]

  initialize() {
    this.url = this.element.getAttribute("data-url")
  }

  connect() {
    this.messagesTarget.textContent = ""
    this.reset()

    this.data.set("scrollToBottom", "true")
    this.scrollToBottom()

    this.loadTimer = setInterval(() => this.load(), 1000)
    this.scrollTimer = setInterval(() => this.scrollToBottom(), 500)

    this.abortEventListener = this.abort.bind(this)
    document.addEventListener('turbolinks:request-start', this.abortEventListener)
  }

  disconnect() {
    clearInterval(this.loadTimer)
    clearInterval(this.scrollTimer)

    this.reset()
    this.abort()
  }

  load() {
    if (this.loadingData) return
    this.loadingData = true

    this.xhr = new XMLHttpRequest()
    this.xhr.seenBytes = 0
    this.xhr.open('GET', `${this.url}?since=${this.timestamp.toISOString()}`)

    this.xhr.onreadystatechange = () => {
      if(this.xhr.readyState > XMLHttpRequest.HEADERS_RECEIVED) {
        this.messagesTarget.textContent += this.xhr.responseText.substr(this.xhr.seenBytes)
        this.xhr.seenBytes = this.xhr.responseText.length

        this.scrollToBottom()
      }

      if (this.xhr.readyState === XMLHttpRequest.DONE) this.reset()
    }

    this.xhr.setRequestHeader("Accept", "text/plain")
    this.xhr.send()
  }

  reset() {
    this.loadingData = false
    this.timestamp = new Date()
  }

  abort() {
    if (this.xhr) this.xhr.abort()

    document.removeEventListener('turbolinks:request-start', this.abortEventListener)
  }

  scrollToBottom(){
    if (this.data.get("scrollToBottom") == "true") {
      this.wrapperTarget.scrollTop = this.wrapperTarget.scrollHeight
      this.buttonTarget.classList.add("active")
    } else {
      this.buttonTarget.classList.remove("active")
    }
  }

  enableScrollToBottom(){
    this.data.set("scrollToBottom", "true")
    this.scrollToBottom()
  }

  disableScrollToBottom(event){
    let scrollTop = this.wrapperTarget.scrollTop
    let scrollHeight = this.wrapperTarget.scrollHeight - this.wrapperTarget.offsetHeight

    if (scrollHeight - scrollTop > 15) {
      this.data.set("scrollToBottom", "false")
    } else {
      this.data.set("scrollToBottom", "true")
    }
    this.scrollToBottom()
  }
}
