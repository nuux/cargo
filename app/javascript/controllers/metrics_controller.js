import { Controller } from "stimulus"
import Chart from 'chart.js'
import moment from 'moment'

export default class extends Controller {
  legend = true

  initialize() {
    this.url = this.element.getAttribute("data-url")

    if (window.innerWidth > 500) {
      this.element.height = 85
    } else {
      this.element.height = 160
    }
    this.element.width = 200

    this.chart = new Chart(this.element.getContext('2d'), {
      type: "line",
      data: {
        datasets: this.datasets.map((options) => this.dataset(options))
      },
      options: {
        legend: {
          display: this.legend
        },
        tooltips: {
          mode: "index",
          intersect: false,
          position: "nearest"
        },
        scales: {
          xAxes: [{
              type: 'time',
              time: {
                unit: 'minute',
                displayFormats: {
                  minute: 'HH:mm'
                },
              }
          }],
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    })
  }

  connect() {
    this.fetchMetrics()
    this.loadTimer = setInterval(() => this.fetchMetrics(), 15000)
  }

  disconnect() {
    clearInterval(this.loadTimer)
  }

  datasets() {
    return []
  }

  dataset(options) {
    return {
      label: options.label,
      backgroundColor: Chart.helpers.color(options.color).alpha(0.1).rgbString(),
      borderColor: options.color,
      borderWidth: 1,
      data: [],
      pointRadius: 1,
      lineTension: 0
    }
  }

  fetchMetrics(callback) {
    this.xhr = new XMLHttpRequest()
    this.xhr.seenBytes = 0
    this.xhr.open('GET', this.url)

    this.xhr.onreadystatechange = () => {
      if(this.xhr.readyState === XMLHttpRequest.DONE) {
        if (this.xhr.status === 200) {
          this.addMetrics(JSON.parse(this.xhr.responseText))
        } else {
          console.error(this.xhr.responseText)
        }
      }
    }

    this.xhr.setRequestHeader("Accept", "application/json")
    this.xhr.send()
  }

  addMetrics(response) {
    for (let dataset of this.chart.data.datasets) {
      dataset.data = []
    }

    for (let result of response.data.result) {
      this.updateDataset(result.metric, result.values)
    }

    this.chart.update()
  }

  findDataset(metric){
    return this.chart.data.datasets[0]
  }

  updateDataset(metric, values) {
    let dataset = this.findDataset(metric)

    let data = values.map((value) => {
      return { x: moment.unix(value[0]), y: Math.round(parseFloat(value[1]) * 10) / 10 }
    })

    dataset.data = data
  }
}
