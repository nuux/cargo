import * as Sentry from '@sentry/browser'
import * as Integrations from '@sentry/integrations'
import { Integrations as TracingIntegrations } from '@sentry/tracing'

import Vue from "vue"

const element = document.head.querySelector("meta[name='sentry']")

let config = null
let user = null

if (element) {
  config = JSON.parse(window.atob(element.getAttribute("content")))

  user = config.user
  delete config.user
}

if (config && config.dsn) {
  config.integrations = [
    new Integrations.Vue({ Vue, attachProps: true }),
    new TracingIntegrations.BrowserTracing()
  ]
  config.tracesSampleRate = 1.0

  Sentry.init(config)

  if (user) {
    Sentry.configureScope((scope) => {
      scope.setUser(user)
    })
  }
}
