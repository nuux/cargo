# frozen_string_literal: true

class DeploymentPolicy < ApplicationPolicy
  def index?
    true
  end

  def show?
    return false unless record.persisted?
    return true if user.admin?

    user.stack_ids.include?(record.stack_id)
  end

  def create?
    true
  end
end
