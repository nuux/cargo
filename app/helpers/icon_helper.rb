# frozen_string_literal: true

module IconHelper
  def embedded_svg(filename, options = {})
    asset = Rails.root.join("app", "assets", "images", filename)
    file = File.read(asset).force_encoding("UTF-8")

    doc = Nokogiri::HTML::DocumentFragment.parse file
    svg = doc.at_css "svg"
    svg["class"] = options[:class] if options[:class].present?

    raw doc # rubocop:disable Rails/OutputSafety
  end

  def icon(name, html_options = {})
    html_options[:class] = "icon icon-#{name} #{html_options[:class]}"
    sprite = html_options.delete(:sprite) || "icons-sprite"

    tag.svg(nil, html_options) do
      tag.use(nil, "xlink:href": "#{image_path("#{sprite}.svg")}##{name}")
    end
  end
end
