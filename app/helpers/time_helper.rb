# frozen_string_literal: true

module TimeHelper
  def time_ago(datetime)
    return if datetime.blank?

    tag.span(data: { toggle: "tooltip", title: l(datetime, format: :short) }) do
      "#{time_ago_in_words(datetime)} ago"
    end
  end
end
