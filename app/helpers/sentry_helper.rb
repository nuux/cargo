# frozen_string_literal: true

module SentryHelper
  def sentry_meta_tag
    return unless Raven.configuration.capture_allowed?

    tag.meta(name: "sentry", content: Base64.strict_encode64(sentry_options.to_json))
  end

private

  def sentry_options
    {
      dsn: "https://#{Raven.configuration.public_key}@#{Raven.configuration.host}/#{Raven.configuration.project_id}",
      release: Raven.configuration.release,
      environment: Rails.env,
      user: {
        id: current_user.try(:id),
        username: current_user.try(:username)
      }.compact.presence
    }
  end
end
