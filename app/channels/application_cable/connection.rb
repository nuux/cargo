# frozen_string_literal: true

module ApplicationCable
  class Connection < ActionCable::Connection::Base
    identified_by :current_user

    def connect
      self.current_user = Current.user = find_current_user
    end

    def find_current_user
      user = User.find_by(id: request.session[:current_user_id])
      return user if user.present?

      reject_unauthorized_connection
    end
  end
end
