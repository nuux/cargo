# frozen_string_literal: true

class ErrorsController < ApplicationController
  layout false

  skip_before_action :verify_authenticity_token
  skip_before_action :authentication!
  after_action -> { request.session_options[:skip] = true }

  def catch_all
    if known_hosts.any? { |known_host| request.host.match?(known_host) }
      service_unavailable
    else
      not_found
    end
  end

  def service_unavailable
    render_status :service_unavailable
  end

  def not_found
    render_status :not_found
  end

private

  def known_hosts
    [
      Frontend.pluck(:hosts).map { |host| host.to_s.split },
      Current.settings.host,
      "*.#{Current.settings.host}"
    ].flatten.reject(&:blank?).map { |host| Regexp.new "^#{Regexp.escape(host).gsub('\*', '.*?')}$", Regexp::IGNORECASE }
  end

  def render_status(code) # rubocop:disable Metrics/MethodLength
    @code = code
    @status = Rack::Utils::SYMBOL_TO_STATUS_CODE[@code]

    respond_to do |format|
      format.json { render_json }
      format.xml  { render_xml }
      format.svg  { render_svg }
      format.js   { render_dynamic }
      format.css  { render_dynamic }
      format.html { render_html }
      format.any  { render_html }
    end
  end

  def render_json
    render json: { errors: [data] }, status: @status
  end

  def render_xml
    render xml: data, status: @status, root: "error", skip_types: true
  end

  def render_svg
    render plain: File.read(Rails.root.join("app", "assets", "images", "#{@code}.svg")).force_encoding("UTF-8"), status: @status
  end

  def render_dynamic
    render :error, status: @status
  end

  def render_html
    render "error.html", status: @status, content_type: "text/html"
  end

  def data
    { code: @code, status: @status.to_s, title: t("errors.#{@code}.title"), detail: helpers.strip_tags(t("errors.#{@code}.detail_html")) }
  end
end
