# frozen_string_literal: true

class UsersController < ApplicationController
  before_action :set_user, only: %i[update destroy]

  def index
    authorize User, :index?

    @users = User.ordered
  end

  def update
    authorize @user, :update?

    if @user.update(user_params)
      head :ok
    else
      render js: "alert('#{ActionController::Base.helpers.j t('.error')}')", status: :unprocessable_entity
    end
  end

  def destroy
    authorize @user, :destroy?

    if @user.destroy
      redirect_to [:users], flash: { success: t(".success") }
    else
      redirect_to [:users], alert: t(".alert")
    end
  end

private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:admin, :approved)
  end
end
