# frozen_string_literal: true

class TemplatesController < ApplicationController
  before_action :set_stack
  before_action :set_template

  def new; end

  def create
    if @template.perform(create_params)
      redirect_to @stack, flash: { success: t(".success") }
    else
      render :new
    end
  end

private

  def set_stack
    @stack = Stack.find(params[:stack_id])

    authorize @stack, :template?
  end

  def set_template
    @template = TemplateService.new(name: params[:name], stack: @stack)
  end

  def create_params
    params.require(:template_service).permit(*@template.inputs.keys)
  end
end
