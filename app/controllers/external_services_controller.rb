# frozen_string_literal: true

class ExternalServicesController < ApplicationController
  before_action :set_stack, only: %i[index new create]
  before_action :set_external_service, only: %i[show edit update destroy]

  def index
    authorize ExternalService, :index?

    @external_services = @stack.external_services.ordered
  end

  def show
    authorize @external_service, :show?
  end

  def new
    @external_service = @stack.external_services.new(service_type: "minio")

    authorize @external_service, :create?
  end

  def edit
    authorize @external_service, :update?
  end

  def create
    @external_service = @stack.external_services.new(external_service_params)

    authorize @external_service, :create?

    if @external_service.save
      redirect_to @external_service, flash: { success: t(".success") }
    else
      render :new
    end
  end

  def update
    authorize @external_service, :update?

    if @external_service.update(external_service_params)
      redirect_to @external_service, flash: { success: t(".success") }
    else
      render :edit
    end
  end

  def destroy
    authorize @external_service, :destroy?

    if @external_service.destroy
      redirect_to [@external_service.stack, :external_services], flash: { success: t(".success") }
    else
      redirect_to @external_service, alert: t(".alert")
    end
  end

private

  def set_stack
    @stack = Stack.find(params[:stack_id])

    authorize @stack, :show?
  end

  def set_external_service
    @external_service = ExternalService.find(params[:id])
    @stack = @external_service.stack

    authorize @stack, :show?
  end

  def external_service_params
    params.require(:external_service).permit(:service_type, :name, :minio_production_bucket_name)
  end
end
