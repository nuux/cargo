# frozen_string_literal: true

module DefaultUrlOptions
  extend ActiveSupport::Concern

  included do
    prepend_before_action :set_default_url_options
  end

private

  def default_url_options
    { host: request.host_with_port, protocol: request.protocol }
  end

  def set_default_url_options
    Rails.application.routes.default_url_options = default_url_options
  end
end
