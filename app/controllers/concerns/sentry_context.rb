# frozen_string_literal: true

module SentryContext
  extend ActiveSupport::Concern

  included do
    before_action :set_raven_context
  end

private

  def set_raven_context
    Raven.user_context raven_user_conext.compact
    Raven.extra_context raven_extra_context.compact
  end

  def raven_user_conext
    { id: current_user.try(:id), username: current_user.try(:username), ip_address: request.remote_ip }
  end

  def raven_extra_context
    { params: params.to_unsafe_h, url: request.url }
  end
end
