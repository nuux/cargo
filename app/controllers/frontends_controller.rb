# frozen_string_literal: true

class FrontendsController < ApplicationController
  before_action :set_stack, only: %i[index new create]
  before_action :set_frontend, only: %i[show edit update destroy]

  def index
    authorize Frontend, :index?

    @frontends = @stack.frontends.ordered
  end

  def show
    authorize @frontend, :show?
  end

  def new
    @frontend = @stack.frontends.new

    authorize @frontend, :create?
  end

  def edit
    authorize @frontend, :update?
  end

  def create
    @frontend = @stack.frontends.new(frontend_params)

    authorize @frontend, :create?

    if @frontend.save
      redirect_to @frontend, flash: { success: t(".success") }
    else
      render :new
    end
  end

  def update
    authorize @frontend, :update?

    if @frontend.update(frontend_params)
      redirect_to @frontend, flash: { success: t(".success") }
    else
      render :edit
    end
  end

  def destroy
    authorize @frontend, :destroy?

    if @frontend.destroy
      redirect_to [@frontend.stack, :frontends], flash: { success: t(".success") }
    else
      redirect_to @frontend, alert: t(".alert")
    end
  end

private

  def set_stack
    @stack = Stack.find(params[:stack_id])

    authorize @stack, :show?
  end

  def set_frontend
    @frontend = Frontend.find(params[:id])
    @stack = @frontend.stack

    authorize @stack, :show?
  end

  def frontend_params
    params.require(:frontend).permit(
      :service_id,
      :name, :port, :hosts, :ignore_wrong_dns_entries, :router_rule,
      :tls, :tls_cert_resolver, :tls_domains, :tls_options,
      :redirect, :redirect_regex, :redirect_replacement, :redirect_permanent,
      :auth_basic, :auth_basic_remove_header, :auth_basic_users,
      :request_headers, :response_headers,
      :hsts, :hsts_max_age, :hsts_include_subdomain
    )
  end
end
