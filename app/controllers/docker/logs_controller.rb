# frozen_string_literal: true

module Docker
  class LogsController < ApplicationController
    include ActionController::Live
    before_action :set_environment
    before_action :set_service

    def index; end

    def stream
      response.headers["Content-Type"] = "text/event-stream"

      stream = LogsStreamService.new(service: @service, stream: response.stream, params: params)

      begin
        stream.run
      rescue IOError, ActionController::Live::ClientDisconnected => exception
        Rails.logger.debug(exception.full_message)
      ensure
        stream.close
      end
    end

  private

    def set_environment
      @environment = Environment.not_deleted.find(params[:environment_id])
      @stack = @environment.stack

      authorize @stack, :show?
      authorize @environment, :logs?
    end

    def set_service
      @service = @environment.docker_services.find { |service| service.id == params[:service_id] }

      raise ActiveRecord::RecordNotFound if @service.blank?
    end
  end
end
