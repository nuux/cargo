# frozen_string_literal: true

class ApplicationSettingsController < ApplicationController
  before_action :set_application_setting, only: %i[show update]

  def show
    authorize @application_setting, :show?
  end

  def update
    authorize @application_setting, :update?

    if @application_setting.update(application_setting_params)
      redirect_to [:application_setting], flash: { success: t(".success") }
    else
      render :show
    end
  end

private

  def set_application_setting
    @application_setting = ApplicationSetting.get
  end

  def application_setting_params
    params.require(:application_setting).permit(
      :allow_creation_of_stacks, :default_production_environment_name, :user_default_approved_regex,
      :host, :frontend_host_schema,
      :primary_color,
      :traefik_version, :traefik_network, :traefik_default_entrypoints, :traefik_cert_resolvers, :backend_name,
      :default_production_node_label, :default_review_node_label,
      :prometheus, :prometheus_host,
      :minio, :minio_host, :minio_access_key, :minio_secret_key,
      node_labels: [], table_options: []
    )
  end
end
