# frozen_string_literal: true

class VolumesController < ApplicationController
  before_action :set_stack, only: %i[index new create]
  before_action :set_volume, only: %i[show edit update destroy]

  def index
    authorize Volume, :index?

    @volumes = @stack.volumes.ordered
  end

  def show
    authorize @volume, :show?
  end

  def new
    @volume = @stack.volumes.new

    authorize @volume, :create?
  end

  def edit
    authorize @volume, :update?
  end

  def create
    @volume = @stack.volumes.new(volume_params)

    authorize @volume, :create?

    if @volume.save
      redirect_to @volume, flash: { success: t(".success") }
    else
      render :new
    end
  end

  def update
    authorize @volume, :update?

    if @volume.update(volume_params)
      redirect_to @volume, flash: { success: t(".success") }
    else
      render :edit
    end
  end

  def destroy
    authorize @volume, :destroy?

    if @volume.destroy
      redirect_to [@volume.stack, :volumes], flash: { success: t(".success") }
    else
      redirect_to @volume, alert: t(".alert")
    end
  end

private

  def set_stack
    @stack = Stack.find(params[:stack_id])

    authorize @stack, :show?
  end

  def set_volume
    @volume = Volume.find(params[:id])
    @stack = @volume.stack

    authorize @stack, :show?
  end

  def volume_params
    params.require(:volume).permit(:name)
  end
end
