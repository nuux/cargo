# frozen_string_literal: true

class ExternalServiceEnvironmentClaim < ApplicationRecord
  attr_encrypted :settings, key: Rails.application.key_generator.generate_key("ExternalServiceEnvironmentClaim#settings", 32), marshal: true

  belongs_to :external_service, optional: true
  belongs_to :environment

  validates :external_service, presence: true, if: :new_record?
  validates :external_service_id, uniqueness: { case_sensitive: false, scope: :environment_id }, if: :will_save_change_to_external_service_id?
  validates :environment_id, uniqueness: { case_sensitive: false, scope: :external_service_id }, if: :will_save_change_to_environment_id?

  before_create :create_minio_bucket, if: :minio?
  before_destroy :remove_minio_bucket, if: :minio?

  def minio?
    return false unless Current.settings.minio?

    external_service&.minio? || settings[:minio]
  end

private

  def create_minio_bucket
    self.settings = ExternalServices::MinioService.create_bucket(bucket_name).merge(minio: true)
  end

  def remove_minio_bucket
    options = settings.symbolize_keys.slice(:access_key, :bucket_name, :policy_name)

    # Don't remove the bucket when it's the production environment
    options[:bucket_name] = nil if environment.production?

    ExternalServices::MinioService.remove_bucket(options)
  end

  def bucket_name
    if environment.production? && external_service.minio_production_bucket_name?
      external_service.minio_production_bucket_name
    else
      "#{environment.docker_name}_#{external_service.name}".dasherize.downcase
    end
  end
end
