# frozen_string_literal: true

class User < ApplicationRecord
  class << self
    def from_omniauth(auth_hash)
      user = find_or_initialize_by(oidc_uid: auth_hash["uid"]) do |new_user|
        new_user.approved = Current.settings.user_default_approved(auth_hash.dig("info", "email").to_s)
      end
      user.username = auth_hash.dig("info", "nickname") || auth_hash.dig("extra", "raw_info", "nickname")
      user.oidc_token = auth_hash.dig("credentials", "token")
      user if user.save
    end
  end

  scope :admin, -> { where(admin: true) }
  scope :approved, -> { where(approved: true) }
  scope :ordered, -> { order(Arel.sql('LOWER("users"."username") ASC')) }

  has_many :stack_members, dependent: :destroy
  has_many :stacks, through: :stack_members

  has_many :services, through: :stacks
  has_many :volumes, through: :stacks
  has_many :frontends, through: :stacks
  has_many :configs, through: :stacks
  has_many :environments, through: :stacks
  has_many :deployments, through: :stacks

  validates :username, presence: true, uniqueness: { case_sensitive: false }
  validates :oidc_uid, presence: true, uniqueness: { case_sensitive: false }
  validates :oidc_token, presence: true, uniqueness: { case_sensitive: false }
  validates :admin, inclusion: { in: [true, false] }
  validates :approved, inclusion: { in: [true, false] }

  alias_attribute :name, :username

  before_create :set_first_user_as_approved_and_admin

private

  def set_first_user_as_approved_and_admin
    return if User.count.positive?

    self.admin = true
    self.approved = true
  end
end
