# frozen_string_literal: true

class Volume < ApplicationRecord
  attr_readonly :name

  has_many :volume_claims, dependent: :restrict_with_error

  belongs_to :stack

  scope :ordered, -> { order(Arel.sql('LOWER("volumes"."name") ASC')) }

  validates :name, presence: true
  validates :name, format: { with: /\A[0-9a-z\-]+\z/i }
  validates :name, uniqueness: { case_sensitive: false, scope: [:stack_id] }
end
