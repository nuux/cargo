# frozen_string_literal: true

class Service < ApplicationRecord
  enum restart_condition: { none: 0, any: 1, "on-failure": 2 }, _prefix: true
  enum rollback_failure_action: { continue: 0, pause: 1 }, _prefix: true
  enum rollback_order: { "stop-first": 0, "start-first": 1 }, _prefix: true
  enum update_failure_action: { continue: 0, pause: 1, rollback: 2 }, _prefix: true
  enum update_order: { "stop-first": 0, "start-first": 1 }, _prefix: true

  attr_encrypted :healthcheck_test, key: Rails.application.key_generator.generate_key("Service#healthcheck_test", 32)

  belongs_to :stack

  has_many :frontends, dependent: :destroy, inverse_of: :service
  has_many :config_claims, dependent: :destroy
  has_many :volume_claims, dependent: :destroy
  has_many :external_service_claims, dependent: :destroy

  scope :ordered, -> { order(Arel.sql('LOWER("services"."name") ASC')) }

  validates :name, presence: true
  validates :name, uniqueness: { case_sensitive: false, scope: [:stack_id] }
  validates :name, format: { with: /\A[0-9a-z\-]+\z/i }
  validates :image, presence: true
  validates :replicas, presence: true, numericality: { only_integer: true, greater_than: 0 }
  validates :restart_condition, presence: true, inclusion: { in: restart_conditions.keys }
  validates :restart_delay, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :restart_window, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :rollback_parallelism, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :rollback_delay, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :rollback_failure_action, presence: true, inclusion: { in: rollback_failure_actions.keys }
  validates :rollback_monitor, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :rollback_order, presence: true, inclusion: { in: rollback_orders.keys }
  validates :update_parallelism, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :update_delay, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :update_failure_action, presence: true, inclusion: { in: update_failure_actions.keys }
  validates :update_monitor, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :update_order, presence: true, inclusion: { in: update_orders.keys }
  validates :healthcheck, inclusion: { in: [true, false] }
  validates :healthcheck_test, presence: true, if: :healthcheck?
  validates :healthcheck_interval, numericality: { only_integer: true, greater_than: 0 }, if: :healthcheck?
  validates :healthcheck_timeout, numericality: { only_integer: true, greater_than: 0 }, if: :healthcheck?
  validates :healthcheck_start_period, numericality: { only_integer: true, greater_than_or_equal_to: 0 }, if: :healthcheck?
  validates :healthcheck_retries, numericality: { only_integer: true, greater_than_or_equal_to: 0 }, if: :healthcheck?

  validates_associated :config_claims
  validates_associated :external_service_claims
  validates_associated :volume_claims

  accepts_nested_attributes_for :config_claims, allow_destroy: true
  accepts_nested_attributes_for :external_service_claims, allow_destroy: true
  accepts_nested_attributes_for :volume_claims, allow_destroy: true

  def duplicate
    duplicated_service = dup
    duplicated_service.name += "-clone"
    duplicated_service.volume_claims = volume_claims.map(&:dup)
    duplicated_service.config_claims = config_claims.map(&:dup)
    duplicated_service.frontends = frontends.map(&:duplicate)
    duplicated_service
  end
end
