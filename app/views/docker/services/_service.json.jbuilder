# frozen_string_literal: true

json.id service.id
json.status service.status
json.name service.name
json.image service.image
json.replicas service.replicas
json.ports service.ports
json.created_at service.created_at
json.updated_at service.updated_at

json.restart restart_environment_service_path(@environment, service.id) if policy(@environment).restart_service?
json.logs environment_service_logs_path(@environment, service.id) if policy(@environment).logs?

json.tasks service.tasks.sort_by(&:created_at).reverse, partial: "docker/tasks/task", as: :task
json.containers service.containers.sort_by(&:created_at).reverse, partial: "docker/containers/container", as: :container
